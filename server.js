//Install express server
const express = require("express");
const path = require("path");

const app = express();

// Serve only the static files form the dist directory
app.use(express.static(__dirname + "/dist/client"));

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname + "/dist/client/index.html"));
});

const port = process.env.APP_TEST_PORT || 1234;

/* eslint-disable no-console */
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
  // models.sequelize.sync({ force: true });
});
