import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantLicenseComponent } from './tenant-license.component';

describe('TenantLicenseComponent', () => {
  let component: TenantLicenseComponent;
  let fixture: ComponentFixture<TenantLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
