import { BehaviorSubject } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TenantsService } from "./../tenants.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { INDUSTRIES } from "src/app/_shared/data/industries";
import { TIMEZONES } from "src/app/_shared/data/timezone";
import { VALIDEMAILREGEX, REMOVESPACESONLY } from "src/app/_shared/utils/utils";
import { UsersService } from "src/app/users/users.service";
import { trigger, transition, style, animate } from "@angular/animations";
import { SharedService } from "src/app/_shared/shared.service";

@Component({
  selector: "app-tenant-list",
  templateUrl: "./tenant-list.component.html",
  styleUrls: ["./tenant-list.component.css"],
  animations: [
    trigger("slideInOut", [
      transition(":enter", [
        style({ transform: "translateY(-100%)" }),
        animate("200ms ease-in", style({ transform: "translateY(0%)" })),
      ]),
      transition(":leave", [
        animate("200ms ease-in", style({ transform: "translateY(-100%)" })),
      ]),
    ]),
  ],
})
export class TenantListComponent implements OnInit {
  template: any = {
    createBilling: false,
    id: {
      createTenant: false,
    },
    options: {
      title: "Organization Setup",
    },
  };
  flow: any = {
    currentId: 0,
    createTenant: {
      options: {
        title: "Organization Setup",
        type: "container",
      },
      tenantId: 0,
      display: false,
    },
    // createBilling: {
    //   options: {
    //     title: "Create Billing",
    //     type: "container",
    //   },
    //   tenantId: 0,
    //   display: false,
    // },
  };
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "Tenant ID", key: "id" },
      { title: "Tenant", key: "name" },
      { title: "Industry", key: "industry" },
      {
        title: "Creation date",
        key: "createdDate",
        // transform: (fieldData, rowData) =>
        //   new Date(fieldData).toLocaleDateString(),
      },
      { title: "Status", key: "planStatus" },
      { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Email", "Sms"],
      singleActions: [
        "View",
        "Create Billing",
        "View Billing",
        // "Message",
        // "View License",
        // "Upgrade",
        //                 "Enable",
        //                 "Disable",
        //                 "Suspend",
      ],
    },
  };
  vm: any = {
    isFound: false,
    status: "Checking license name...",
  };
  display: any = {};
  arrayTenants: any = [];
  arrayUsers: any = [];
  arrayIndustry: any = [];
  arrayTimezone: any = [];
  createOrgForm: FormGroup;
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: any;
  showModal: boolean;
  modalSpinnerType: string;
  modalSpinnerStatus: string;
  showModalSpinner: boolean;
  isLoading: boolean = false;
  showModalSuccess: boolean = false;
  loader: any = {
    btn: {
      addTenant: false,
    },
  };
  dataFilter = {
    dataChangedObs: new BehaviorSubject(null),
    accordions: [
      // { name: "Tenant Name", type: "box", search: true, filterKey: "name" },
      { name: "Industry", type: "box", search: true, filterKey: "industry" },
      {
        name: "Status",
        type: "box",
        search: false,
        filterKey: "planStatus",
      },
      {
        name: "Created Date",
        type: "date",
        filterKey: "createdDate",
        icon: "fa fa-calendar",
      },
    ],
  };
  currentTimeZoneObj;

  constructor(
    private tenantsService: TenantsService,
    private usersService: UsersService,
    private sharedService: SharedService,
    private toastr: ToastrService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.arrayIndustry = INDUSTRIES;
    this.arrayTimezone = TIMEZONES;

    let currentTimeZone = new Date().getTimezoneOffset() / -60;
    let currentTimeZoneText = this.seconds_with_leading_zeros(new Date());
    let timeZoneSubstr = currentTimeZoneText.substr(
      currentTimeZoneText.indexOf(""),
      currentTimeZoneText.indexOf(" ")
    );

    this.currentTimeZoneObj = this.arrayTimezone.find(
      (x) => x.text.includes(timeZoneSubstr) && x.offset == currentTimeZone
    );

    console.log(this.currentTimeZoneObj);

    this.display.nav = true;
  }

  ngOnInit() {
    this.createForm();
    this.getAllOrganizationsAsync();
    this.getAllUsersAsync();
  }

  dataFeedBackObsListener = (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        if (data.action === "View") {
          this.router.navigate(["/tenants/status/" + data.data.id]);
        } else if (data.action === "Create Billing") {
          this.flow.createBilling.options.title = `Create Billing | ${data.data.name}`;
          this.flow.createBilling.tenantId = data.data.id;
          this.openTemplate("custom-modal-2");
        } else if (data.action === "View License") {
          this.router.navigate(["/tenants/license/" + data.data.id]);
        } else if (data.action === "Delete") {
          // @ts-ignore
          document.querySelector("[data-target='#deleteLicenseModal'").click();
          // this.onModalDelete(data.data.id)
        }
        break;

      default:
        break;
    }
  };

  async dataSourceListener(event) {
    console.log(event.data);

    switch (event.action) {
      case "filter":
        this.arrayTenants = event.data;
        this.dataTable.dataChangedObs.next(true);
        break;

      case "clear":
        // Loader here
        await this.getAllOrganizationsAsync();
        break;

      default:
        break;
    }
  }

  async eventSource(event) {
    console.log(event.data);

    switch (event.action) {
      case "createInvoiceSuccess":
        this.toastr.success("Invoice created successfully!", "Success");
        this.closeTemplate(this.flow.currentId);
        this.dataTable.dataChangedObs.next(true);
        break;

      case "createInvoiceFailed":
        this.toastr.error("Error updating invoice.", "Please try again!");
        this.closeTemplate(this.flow.currentId);
        this.dataTable.dataChangedObs.next(true);
        break;

      default:
        break;
    }
  }

  openTemplate(id: string) {
    console.log(id);
    console.log(this.flow.currentId, "id");
    this.flow.currentId = id;
    this.createOrgForm.reset();
    this.sharedService.open(id);
  }

  closeTemplate(id: string) {
    this.sharedService.close(id);
  }

  get f() {
    return this.createOrgForm.controls;
  }

  seconds_with_leading_zeros(dt) {
    return /\((.*)\)/.exec(new Date().toString())[1];
  }

  // Create form
  createForm() {
    this.createOrgForm = this.fb.group({
      name: ["", [Validators.required, REMOVESPACESONLY]],
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(VALIDEMAILREGEX),
        ]),
      ],
      industry: [null, Validators.required],
      // timezone: [this.currentTimeZoneObj.offset, Validators.required],
      timezone: [null, Validators.required],
      userId: [null, Validators.required],
    });
  }

  async getAllOrganizationsAsync() {
    try {
      this.loader.main = true;
      this.arrayTenants = await this.tenantsService
        .getAllOrganizations()
        .toPromise();
      console.log(this.arrayTenants, "arrayTenants");
    } catch (error) {
      console.log(error, "error");
    } finally {
      this.dataFilter.dataChangedObs.next(true);
      this.dataTable.dataChangedObs.next(true);
      this.loader.main = false;
    }
  }

  // Initialize Users
  async getAllUsersAsync() {
    try {
      this.arrayUsers = await this.usersService.getUsers().toPromise();
      this.arrayUsers.map((i) => {
        i.fullName = i.firstName + " " + i.lastName;
        return i;
      });
      console.log(this.arrayUsers, "arrayUsers");
    } catch (error) {
      console.log(error, "user error");
    }
  }

  // Filter License
  async onValidateName(input) {
    // let name = this.f.name.value;
    let name = input;
    console.log(name, "name");

    this.vm.isFound =
      this.arrayTenants.filter(
        (e) => e.name.toLowerCase() === name.toLowerCase()
      ).length > 0;
    // console.log(this.vm.isFound, "isFound");
  }

  async onCreateTenant() {
    try {
      this.loader.btn.addTenant = true;
      const payload = this.createOrgForm.value;
      console.log(payload, "payload");
      await this.tenantsService.newOrganization(payload).toPromise();
      this.closeTemplate(this.flow.currentId);
      // this.toastr.success("Tenant created successfully!", "Success");
      await this.getAllOrganizationsAsync();
    } catch (err) {
      let errMsg = "Error occured try again";
      errMsg = err.error && err.error.message ? err.error.message : errMsg;
      this.toastr.error(errMsg);
    } finally {
      this.loader.btn.addTenant = false;
      await this.getAllUsersAsync();
    }
  }

  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Retrying...";
    // this.ngOnInit();

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }
}
