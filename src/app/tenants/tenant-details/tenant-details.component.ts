import { TenantsService } from './../tenants.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tenant-details',
  templateUrl: './tenant-details.component.html',
  styleUrls: ['./tenant-details.component.css']
})

export class TenantDetailsComponent implements OnInit {

  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataObject: any;
  filter: any = {};

  constructor(
    private tenantsService: TenantsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Loading...";
  }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.getAdminTenant(id);
  }

  getAdminTenant(orgID) {
    this.tenantsService
      .getAdminTenant(orgID)
      .subscribe((data: any) => {
        console.log(data, 'orgID data');
        this.dataObject = data;
        this.showSpinner = false;
      }, (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load this view.";
      });
  }

  onChange() {
    let users = this.dataObject.users;
    if (this.filter.search) {
      users = users.filter(v => v.email == this.filter.search);
    }
    this.dataObject.users = users;
  }

  resetFilter() {
    this.filter = {};
    this.onChange();
  }


  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Retrying...";
    // this.ngOnInit();

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }

}
