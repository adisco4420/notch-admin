import { Injectable } from "@angular/core";
import { AppService } from "../app.service";
import { retry, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class TenantsService {
  url: string;

  constructor(private appService: AppService) {}

  getAllOrganizations() {
    this.url = `${this.appService.signUpURL}/getAllOrganizations/`;
    return this.appService
      .get(this.url)
      .pipe(retry(1), catchError(this.appService.handleError));
  }

  newOrganization(payload) {
    this.url = `${this.appService.signUpURL}/newOrganization/`;
    return this.appService.post(this.url, payload);
  }

  getAdminTenant(orgID: number) {
    this.url = `${this.appService.signUpURL}/getAdminTenant/${orgID}/`;
    return this.appService
      .get(this.url)
      .pipe(retry(1), catchError(this.appService.handleError));
  }
}
