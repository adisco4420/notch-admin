import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { TenantListComponent } from './tenant-list/tenant-list.component';
import { TenantDetailsComponent } from './tenant-details/tenant-details.component';
import { TenantLicenseComponent } from './tenant-license/tenant-license.component';
import { EditTenantComponent } from './edit-tenant/edit-tenant.component';


const routes: Routes = [
  {
    path: 'all',
    component: TenantListComponent,
  },
  {
    path: 'edit/:id',
    component: EditTenantComponent,
  },
  {
    path: 'status/:id',
    component: TenantDetailsComponent,
  },
  {
    path: 'license/:id',
    component: TenantLicenseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenantsRoutingModule { }
