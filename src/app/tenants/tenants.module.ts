import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TenantsRoutingModule } from "./tenants-routing.module";
import { EditTenantComponent } from "./edit-tenant/edit-tenant.component";
import { TenantLicenseComponent } from "./tenant-license/tenant-license.component";
import { TenantDetailsComponent } from "./tenant-details/tenant-details.component";
import { TenantListComponent } from "./tenant-list/tenant-list.component";
import { SharedModule } from "../_shared/shared.module";
import { FormsModule } from "@angular/forms";
import { BillingInvoiceModule } from "../billing-invoice/billing-invoice.module";

@NgModule({
  declarations: [
    TenantListComponent,
    EditTenantComponent,
    TenantLicenseComponent,
    TenantDetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    TenantsRoutingModule,
    BillingInvoiceModule,
  ],
  exports: [
    TenantListComponent,
    EditTenantComponent,
    TenantLicenseComponent,
    TenantDetailsComponent,
  ],
})
export class TenantsModule {}
