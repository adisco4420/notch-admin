import { Injectable } from "@angular/core";
import { AppService } from "../app.service";
import { mergeMap } from "rxjs/operators";
import { of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SettingsService {
  constructor(
    private appService: AppService // store: Store<AppState>,
  ) {
    // store.select("userInfo").subscribe((info) => {
    //   if (info) {
    //     this.userSubject.next(info);
    //     this.orgID = info.organization ? info.organization.id : null;
    //     this.org = info.organization ? info.organization : null;
    //     this.supervisor = info.supervisor ? info.supervisor : null;
    //     this.user = info.user ? info.user : null;
    //     this.token = info.token ? info.token : null;
    //     // this.priviledges = info.priviledges ? info.priviledges : [];
    //     // this.roleName = info.roleName ? info.roleName : null;
    //   }
    //   console.log("gs-rolename", this.roleName);
    // });
  }

  // Bank Payment
  getAllBankPayments() {
    let url = `${this.appService.baseURL}/getAllBankPayments/`;
    return this.appService.get(url);
  }

  getAllOrganizationBillingPayment(orgId) {
    let url = `${this.appService.baseURL}/${orgId}/getAllOrganizationBillingPayment/`;
    return this.appService.get(url);
  }

  newPayment(payload) {
    let url = `${this.appService.baseURL}/newPayment/`;
    return this.appService.post(url, payload);
  }

  editBankPayment(payload) {
    let url = `${this.appService.baseURL}/editBankPayment/`;
    return this.appService.post(url, payload);
  }

  deleteBankPayment(id: number) {
    let url = `${this.appService.baseURL}/${id}/deleteBankPayment/`;
    return this.appService.post(url, id);
  }

  // Bank Setup
  getAllBankAccounts() {
    let url = `${this.appService.baseURL}/getAllBankAccounts/`;
    return this.appService.get(url);
  }

  getBankAccount(id: number) {
    let url = `${this.appService.baseURL}/${id}/getBankAccount/`;
    return this.appService.get(url);
  }

  newBankAccount(payload) {
    let url = `${this.appService.baseURL}/newBankAccount/`;
    return this.appService.post(url, payload);
  }

  editBankAccount(payload) {
    let url = `${this.appService.baseURL}/editBankAccount/`;
    return this.appService.post(url, payload);
  }

  deleteBankAccount(id: number) {
    let url = `${this.appService.baseURL}/${id}/deleteBankAccount/`;
    return this.appService.post(url, id);
  }

  // Card
  getBankPaymentsByBillingID(id: number) {
    let url = `${this.appService.baseURL}/${id}/getBankPaymentsByBillingID/`;
    return this.appService.get(url);
  }

  chargeCardForBill(billID: number, cardID: number) {
    let url = `${this.appService.baseURL}/${billID}/${cardID}/chargeCardForBill/`;
    return this.appService.get(url);
  }

  /* -------------------------------
      USER ACCESS METHODS BEGINS
  ----------------------------------*/

  getAdminUsers() {
    const url = `${this.appService.baseURL}/getAdminUsers/`;
    return this.appService.get(url).pipe(
      mergeMap((data: any[]) => {
        const result = data.map((dat) => ({
          ...dat.adminUser,
          role: dat.roles[0],
        }));
        return of(result);
      })
    );
  }

  getAdminUsersByStatus(id) {
    const url = `${this.appService.baseURL}/${id}/getAdminUsersByStatus/`;
    return this.appService.get(url);
  }

  inviteAdminUser(payload) {
    const url = `${this.appService.baseURL}/inviteAdminUser/`;
    return this.appService.post(url, payload);
  }
}
