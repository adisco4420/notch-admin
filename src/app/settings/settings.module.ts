import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BankPaymentComponent } from "./bank/bank-payment/bank-payment.component";
import { BankSetupComponent } from "./bank/bank-setup/bank-setup.component";
import { UserProfileComponent } from "./user/user-profile/user-profile.component";
import { UserRolesComponent } from "./user/user-roles/user-roles.component";
import { UserSettingsComponent } from "./user/user-settings/user-settings.component";
import { SettingsRoutingModule } from "./settings-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../_shared/shared.module";
import { LaddaModule } from "angular2-ladda";
import { ChangePwdComponent } from './user/change-pwd/change-pwd.component';

@NgModule({
  declarations: [
    BankPaymentComponent,
    BankSetupComponent,
    UserProfileComponent,
    UserRolesComponent,
    UserSettingsComponent,
    ChangePwdComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    SettingsRoutingModule,
    LaddaModule,
  ],
})
export class SettingsModule {}
