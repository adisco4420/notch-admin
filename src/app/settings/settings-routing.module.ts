import { ChangePwdComponent } from './user/change-pwd/change-pwd.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BankSetupComponent } from "./bank/bank-setup/bank-setup.component";
import { BankPaymentComponent } from "./bank/bank-payment/bank-payment.component";
import { UserRolesComponent } from "./user/user-roles/user-roles.component";
import { UserSettingsComponent } from "./user/user-settings/user-settings.component";
import { UserProfileComponent } from "./user/user-profile/user-profile.component";

const routes: Routes = [
  { path: "", redirectTo: "user-centre", pathMatch: "full" },
  {
    path: "user-centre",
    component: UserSettingsComponent,
  },
  {
    path: "user-profile",
    component: UserProfileComponent,
  },
  {
    path: "change-password",
    component: ChangePwdComponent,
  },
  {
    path: "user-roles",
    component: UserRolesComponent,
  },
  {
    path: "bank-setup",
    component: BankSetupComponent,
  },
  {
    path: "bank-payment",
    component: BankPaymentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsRoutingModule {}
