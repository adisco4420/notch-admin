import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { VALIDEMAILREGEX, REMOVESPACESONLY } from "src/app/_shared/utils/utils";
import { BehaviorSubject } from "rxjs";
import { ROLES } from "src/app/_shared/data/roles";
import { SettingsService } from "../../settings.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-user-settings",
  templateUrl: "./user-settings.component.html",
  styleUrls: ["./user-settings.component.css"],
})
export class UserSettingsComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),

    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "First Name", key: "firstName" },
      { title: "Last Name", key: "lastName" },
      { title: "Email", key: "email" },
      {
        title: "Role",
        key: "role",
        transform: (fielddata) => `${fielddata ? fielddata : "Nil"}`,
      },
      { title: "Action", key: "action" },
    ],
    options: {
      datePipe: {},
      singleActionsIf: {
        Deactivate: "status",
        Activate: "!status",
      },
      singleActions: [
        {
          title: "Edit User",
          showIf: (a, b) => {
            // return this.gs.isAuthorized("ADMIN_USER_ACTIONS");
          },
        },
        {
          title: "Deactivate",
          showIf: (a, b) => {
            // return this.gs.isAuthorized("ADMIN_USER_ACTIONS") && b.status;
          },
        },
        {
          title: "Activate",
          showIf: (a, b) => {
            // return this.gs.isAuthorized("ADMIN_USER_ACTIONS") && !b.status;
          },
        },
      ],
      bulkActions: [],
    },
  };
  userForm: FormGroup;
  arrayRoles: any;
  arrayUsers: any = [];
  loader: any = {};

  constructor(
    public router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private settingSvc: SettingsService
  ) {
    this.arrayRoles = ROLES;
  }

  async ngOnInit() {
    this.ngForms();
    await this.getAllUsersAsync();
  }

  dataFeedBackObsListener = (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        if (data.action === "View") {
          this.router.navigate(["/tenants/status/" + data.data.id]);
        } else if (data.action === "Create Billing") {
        } else if (data.action === "View License") {
          this.router.navigate(["/tenants/license/" + data.data.id]);
        } else if (data.action === "Delete") {
          // @ts-ignore
          document.querySelector("[data-target='#deleteLicenseModal'").click();
          // this.onModalDelete(data.data.id)
        }
        break;

      default:
        break;
    }
  };

  get u() {
    return this.userForm.controls;
  }

  ngForms() {
    // Add User Form
    this.userForm = this.fb.group({
      firstName: ["", [Validators.required, REMOVESPACESONLY]],
      lastName: ["", [Validators.required, REMOVESPACESONLY]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern(VALIDEMAILREGEX),
          REMOVESPACESONLY,
        ],
      ],
      roles: ["", Validators.required],
    });
  }

  async getAllUsersAsync() {
    try {
      this.arrayUsers = await this.settingSvc.getAdminUsers().toPromise();
      this.dataTable.dataChangedObs.next(this.arrayUsers);
    } catch (error) {
      console.log(error);
    }
  }

  // async loadUserByOrg(meta?) {
  //   await this.orgServ
  //     .getUsersInOrganization()
  //     .toPromise()
  //     .then((result: any) => {
  //       this.usersList = result;
  //       this.dataTable.dataChangedObs.next(true);
  //     });
  // }

  async onInviteUser() {
    try {
      this.loader.invite = true;
      let data = { ...this.userForm.value };
      const payload = {
        adminUser: {
          email: data.email,
          firstName: data.firstName,
          lastName: data.lastName,
        },
        roles: data.roles,
      };
      await this.settingSvc.inviteAdminUser(payload).toPromise();
      this.toastr.success("Invitation sent successfully!");
      console.log(payload, "payload");
    } catch (error) {
      console.log(error, "error");
      this.toastr.error("Invitation failed! Please try again", "Error");
      // @ts-ignore
      document.querySelector("#inviteModal .close").click();
    } finally {
      // @ts-ignore
      document.querySelector("#inviteModal .close").click();
      await this.getAllUsersAsync();
      this.dataTable.dataChangedObs.next(true);
      this.loader.invite = false;
    }
  }
}
