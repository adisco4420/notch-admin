import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { SharedService } from "src/app/_shared/shared.service";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "./../../../auth/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-change-pwd",
  templateUrl: "./change-pwd.component.html",
  styleUrls: ["./change-pwd.component.css"],
})
export class ChangePwdComponent implements OnInit {
  dto = { email: "", npassword: "", password: "" };
  confirmPassword = "";
  submitting = false;
  constructor(
    private authSrv: AuthService,
    private toastr: ToastrService,
    private gs: SharedService,
    private router: Router,
    route: ActivatedRoute
  ) {
    const email = route.snapshot.queryParamMap.get("email");
    if (email) {
      this.dto.email = email;
    }
  }

  ngOnInit() {}

  get matchPassword() {
    const vaildPasswordInput = this.dto.npassword !== this.confirmPassword;
    return vaildPasswordInput && this.dto.npassword && this.confirmPassword;
  }

  get disableBtn() {
    const validState =
      this.dto.password && this.dto.npassword && this.confirmPassword;
    const vaildMatchPassword = this.dto.npassword === this.confirmPassword;
    return !(vaildMatchPassword && validState);
  }

  changePwd() {
    const body = { ...this.dto };
    this.submitting = true;
    this.authSrv.changePwd(body).subscribe(
      (res: any) => {
        this.toastr.success(res.message);
        this.reInitialize();
        if (this.dto.email) {
          this.toastr.success("Please login with your new password");
          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 2000);
        }
      },
      (err) => {
        this.toastr.error(this.gs.getErrMsg(err));
        this.submitting = false;
      }
    );
  }

  private reInitialize() {
    this.dto.password = null;
    this.confirmPassword = null;
    this.dto.npassword = null;
    this.submitting = false;
  }
}
