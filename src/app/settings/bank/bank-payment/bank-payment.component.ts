import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/auth/auth.service";
import { SettingsService } from "../../settings.service";
import { SharedService } from "src/app/_shared/shared.service";
import { LicenseService } from "src/app/license/license.service";
import { TenantsService } from "src/app/tenants/tenants.service";
import { BillingInvoiceService } from "src/app/billing-invoice/billing-invoice.service";

@Component({
  selector: "app-bank-payment",
  templateUrl: "./bank-payment.component.html",
  styleUrls: ["./bank-payment.component.css"],
})
export class BankPaymentComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "Id", key: "reference" },
      {
        title: "Amount",
        key: "amount",
        transform: (fieldData, rowData) =>
          (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
      },
      { title: "Method", key: "whatMode" },
      { title: "Bank", key: "bank" },
      { title: "Account Name", key: "acctName" },
      { title: "Account No", key: "acctNo" },
      {
        title: "Created at",
        key: "createdDate",
        transform: (fieldData, rowData) => {
          if (fieldData) {
            return new Date(fieldData).toLocaleDateString();
          }
        },
      },
      {
        title: "Paid at",
        key: "paymentDate",
        transform: (fieldData, rowData) => {
          if (fieldData) {
            return new Date(fieldData).toLocaleDateString();
          }
        },
      },
      {
        title: "Status",
        key: "paymentStatus",
        transform: (fieldData, rowData) => {
          if (fieldData === 0) {
            return (fieldData = "Pending");
          } else if (fieldData === 1) {
            return (fieldData = "Confirmed");
          }
        },
      },
    ],
    options: {
      bulkActions: ["Export"],
      // singleActions: ["View/Edit", "View License"],
    },
  };
  dataFilter = {
    dataChangedObs: new BehaviorSubject(null),
    accordions: [
      { name: "License Name", type: "box", search: true, filterKey: "name" },
      // { name: "Group", type: "box", search: true, filterKey: "groupName" },
      {
        name: "Max Users",
        type: "range",
        filterKey: "maxUsers",
        icon: "fa fa-user",
      },
      {
        name: "Monthly Price",
        type: "range",
        filterKey: "monthlyPrice",
        // icon: "fa fa-ticket-alt",
      },
      {
        name: "Annual Price",
        type: "range",
        filterKey: "annuallyPrice",
        // icon: "fa fa-ticket-alt",
      },
      {
        name: "Max Storage",
        type: "range",
        filterKey: "maxStorage",
        // icon: "fa fa-ticket-alt",
      },
    ],
  };
  loader: any = {
    main: {
      type: "sharp",
      status: "Loading...",
      styles: { marginTop: "10%" },
      showSpinner: false,
    },
    export: false,
  };
  dataSource: any = [];

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private settingSvc: SettingsService,
    private sharedService: SharedService,
    private licenseService: LicenseService,
    private tenantsService: TenantsService,
    private billingInvoiceService: BillingInvoiceService,
    public router: Router
  ) {}

  async ngOnInit() {
    await this.getAllBankPaymentsAsync();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        // if (data.action === "View") {
        //   this.router.navigate(["/license/view/" + data.data.id]);
        // } else if (data.action === "Edit") {
        //   this.router.navigate(["/license/edit/" + data.data.id]);
        // }
        break;
      case "bulkactions":
        if (data.action === "Export") await this.exportTable();
        break;

      default:
        break;
    }
  };

  async dataSourceListener(event) {
    console.log(event.data);

    switch (event.action) {
      case "filter":
        // this.dataSource = event.data;
        this.dataTable.dataChangedObs.next(true);
        break;

      case "createFilter":
        // await this.createCustomFilterAsync(event.data);
        // await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "updateFilter":
        // await this.updateCustomFilterAsync(event.data);
        // await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "processFilter":
        // await this.processCustomFilterAsync(event.data);
        this.dataTable.dataChangedObs.next(true);
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "deleteFilter":
        // await this.deleteCustomFilterByIdAsync(event.data);
        // await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "clear":
        // Loader here
        // await this.initializeLicenses();
        break;

      default:
        break;
    }
  }

  async getAllBankPaymentsAsync() {
    try {
      this.loader.main.showSpinner = true;
      this.dataSource = await this.settingSvc.getAllBankPayments().toPromise();
      console.log(this.dataSource);
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    } finally {
      this.dataTable.dataChangedObs.next(true);
      this.dataFilter.dataChangedObs.next(true);
      this.loader.main.showSpinner = false;
    }
  }

  async exportTable() {
    return;
  }
}
