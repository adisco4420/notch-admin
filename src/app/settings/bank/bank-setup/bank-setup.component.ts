import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { SettingsService } from "../../settings.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-bank-setup",
  templateUrl: "./bank-setup.component.html",
  styleUrls: ["./bank-setup.component.css"],
})
export class BankSetupComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "Bank", key: "bank" },
      { title: "Account Number", key: "accountNo" },
      { title: "Account Name", key: "accountName" },
      // { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Export"],
      // singleActions: ["Edit", "Delete"],
    },
  };
  addBankForm: FormGroup;
  arrayBanks: any = [];
  loader: any = {};

  constructor(
    public router: Router,
    private fb: FormBuilder,
    private svc: SettingsService,
    private toastr: ToastrService
  ) {}

  async ngOnInit() {
    this.ngOnForms();
    await this.getAllBankAccountsAsync();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        if (data.action === "Edit") {
          await this.editBankAccount(data);
        } else if (data.action === "Delete") {
          await this.deleteBankAccount(data);
        }
        break;
      case "bulkactions":
        if (data.action === "Export") await this.exportTable();
        break;

      default:
        break;
    }
  };

  // Get Bank Form Value
  get bf() {
    return this.addBankForm.controls;
  }

  ngOnForms() {
    this.addBankForm = this.fb.group({
      bank: ["", Validators.required],
      accountNo: ["", Validators.required],
      accountName: ["", Validators.required],
    });
  }

  async onCreateAccount() {
    try {
      this.loader.add = true;
      const payload = { ...this.addBankForm.value };
      console.log(payload, "payload");
      await this.svc.newBankAccount(payload).toPromise();
      await this.getAllBankAccountsAsync();
      this.toastr.success("Account added successfully!");
    } catch (error) {
      console.log(error);
      this.toastr.error("Account added successfully!");
    } finally {
      this.loader.add = false;
      this.dataTable.dataChangedObs.next(true);
    }
  }

  async getAllBankAccountsAsync() {
    try {
      this.loader.main = true;
      this.arrayBanks = await this.svc.getAllBankAccounts().toPromise();
    } catch (error) {
      console.log(error);
    } finally {
      this.loader.main = false;
      this.dataTable.dataChangedObs.next(true);
    }
  }

  async editBankAccount(payload) {
    try {
      this.loader.add = true;
      let data = await this.svc.editBankAccount(payload).toPromise();
    } catch (error) {
      console.log(error);
    } finally {
      this.loader.add = false;
      this.dataTable.dataChangedObs.next(true);
    }
  }

  async deleteBankAccount(payload) {
    try {
      const { id } = payload;
      let data = await this.svc.deleteBankAccount(id).toPromise();
    } catch (error) {
      console.log(error);
    }
  }

  exportTable() {
    return;
  }
}
