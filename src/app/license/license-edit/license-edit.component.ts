import { Component, OnInit } from "@angular/core";
import { LicenseService } from "../license.service";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormArray,
  ValidatorFn,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { License } from "../license.model";
import { modulesTable } from "src/app/_shared/modules";
import { ToastrService } from "ngx-toastr";
import { SharedService } from "src/app/_shared/shared.service";
import { REMOVESPACESONLY } from "src/app/_shared/utils/utils";

@Component({
  selector: "app-license-edit",
  templateUrl: "./license-edit.component.html",
  styleUrls: ["./license-edit.component.css"],
})
export class LicenseEditComponent implements OnInit {
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  isLoading: boolean = false;

  lid: number;
  licenseForm: FormGroup;
  licenseData: License;
  arrayModules: any;
  numberOfModules = 0;
  selectedControl = new FormControl();
  isTrialPeriod: boolean = false;

  constructor(
    private licenseService: LicenseService,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Fetching data...";
    this.arrayModules = modulesTable;
  }

  ngOnInit() {
    this.getModules();
    this.createForm();

    this.route.params.subscribe(async (params) => {
      this.lid = params["id"];
      await this.getLicenseById(this.lid);
      this.checkTrialVersion();
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.licenseForm.controls;
  }

  getModules() {
    this.sharedService.getSectionModules(0).subscribe((data) => {
      this.arrayModules = data;
    });
  }

  async getLicenseById(id) {
    try {
      const data: any = await this.licenseService
        .getLicenseById(id)
        .toPromise();
      console.log(data, "data");

      this.setFormValues(data);
      this.licenseData = data;
    } catch (error) {
      this.toastr.error(error);
    } finally {
      this.showSpinner = false;
    }
  }

  setFormValues(data) {
    this.licenseForm.patchValue({
      name: data.name,
      descrip: data.descrip,
      maxUsers: data.maxUsers,
      monthlyPrice: data.monthlyPrice,
      monthlyPriceUSD: data.monthlyPriceUSD,
      quarterlyPrice: data.quarterlyPrice,
      quarterlyPriceUSD: data.quarterlyPriceUSD,
      annuallyPrice: data.annuallyPrice,
      annuallyPriceUSD: data.annuallyPriceUSD,
      maxStorage: data.maxStorage,
      trialPeriod: data.trialPeriod,
      // modules: data.modules,
    });
    this.autoCheckboxes(data.modules);
  }

  createForm() {
    this.licenseForm = this.fb.group({
      name: ["", [Validators.required, REMOVESPACESONLY]],
      descrip: ["", [Validators.required, REMOVESPACESONLY]],
      maxUsers: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      monthlyPrice: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      monthlyPriceUSD: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      quarterlyPrice: [
        1,
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      quarterlyPriceUSD: [
        1,
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      annuallyPrice: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      annuallyPriceUSD: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      maxStorage: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      modules: new FormArray([], this.minSelectedCheckboxes(1)),
      trialPeriod: [0, [Validators.pattern("^[0-9]*$")]],
    });
  }

  checkTrialVersion() {
    if (this.licenseData !== null)
      if (this.f.trialPeriod.value > 0) {
        // console.log(this.f.trialPeriod.value, "trialPeriod");
        this.isTrialPeriod = true;
      }
  }

  onSubmit() {
    this.isLoading = true;
    const selectedModules = this.licenseForm.value.modules
      .map((v, i) => (v ? this.arrayModules[i] : null))
      .filter((v) => v !== null);

    this.licenseForm.value.modules = selectedModules;
    this.numberOfModules = selectedModules.length;
    this.processForm();
  }

  processForm() {
    let payload = this.licenseForm.value;
    payload = { ...payload, id: this.licenseData.id };
    this.licenseService.editLicense(payload).subscribe((res) => {
      this.router.navigate(["license/list"]);
    });
  }

  private autoCheckboxes(modules) {
    this.arrayModules.forEach((value, key) => {
      // let control;
      let found = false;
      for (let i in modules) {
        if (modules[i] == value) {
          found = true;
          break;
        }
      }

      const control = new FormControl(found);
      (this.licenseForm.controls.modules as FormArray).push(control);
    });
  }

  private addCheckboxes() {
    this.arrayModules.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.licenseForm.controls.modules as FormArray).push(control);
    });
  }

  private minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map((control) => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => (next ? prev + next : prev), 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }
}
