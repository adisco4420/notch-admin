import { BehaviorSubject, Observable, combineLatest } from "rxjs";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { LicenseService } from "../license.service";
import { Router, NavigationEnd } from "@angular/router";
import { License } from "../license.model";
import { DatePipe } from "@angular/common";
import { exportTableToCSV } from "src/app/_shared/utils/utils";
import { map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SharedService } from "src/app/_shared/shared.service";
declare var $: any;

@Component({
  selector: "app-license-list",
  templateUrl: "./license-list.component.html",
  styleUrls: ["./license-list.component.css"],
})
export class LicenseListComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "License name", key: "name" },
      { title: "Maximum user", key: "maxUsers" },
      { title: "Monthly price", key: "monthlyPrice" },
      // { title: "Quarterly price", key: "quarterlyPrice" },
      { title: "Annual price", key: "annuallyPrice" },
      { title: "Max storage", key: "maxStorage" },
      { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Export"],
      singleActions: ["View", "Edit"],
    },
  };
  dataFilter = {
    dataChangedObs: new BehaviorSubject(null),
    accordions: [
      { name: "License Name", type: "box", search: true, filterKey: "name" },
      // { name: "Group", type: "box", search: true, filterKey: "groupName" },
      {
        name: "Max Users",
        type: "range",
        filterKey: "maxUsers",
        icon: "fa fa-user",
      },
      {
        name: "Monthly Price",
        type: "range",
        filterKey: "monthlyPrice",
        // icon: "fa fa-ticket-alt",
      },
      {
        name: "Annual Price",
        type: "range",
        filterKey: "annuallyPrice",
        // icon: "fa fa-ticket-alt",
      },
      {
        name: "Max Storage",
        type: "range",
        filterKey: "maxStorage",
        // icon: "fa fa-ticket-alt",
      },
    ],
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: any = [];
  singleLicense: License;
  showModal: boolean;
  mySubscription: any;
  loader: any = {
    dataless: {
      title: "No Data Available!",
      subTitle: "Please create a contact and try again",
      action: "Create Contact",
      success: true,
      exists: true,
    },
    main: {
      type: "sharp",
      status: "Loading...",
      showSpinner: false,
    },
    export: false,
  };
  modalSpinnerType: string;
  modalSpinnerStatus: string;
  showModalSpinner: boolean;
  isLoading: boolean = false;
  showModalSuccess: boolean = false;
  customFilters;
  filter: any = {
    nameSearch: "",
    nameCheckbox: {},
    data: {
      name: [{}],
    },
    source: [{}],
  };

  constructor(
    private router: Router,
    private datepipe: DatePipe,
    private toastr: ToastrService,
    private sharedSvc: SharedService,
    private licenseService: LicenseService
  ) {
    this.loader.main.type = "sharp";
    this.modalSpinnerType = "jsBin";
    this.modalSpinnerStatus = "";
  }

  async ngOnInit() {
    await this.initializeLicenses();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        if (data.action === "View") {
          this.router.navigate(["/license/view/" + data.data.id]);
        } else if (data.action === "Edit") {
          this.router.navigate(["/license/edit/" + data.data.id]);
        }
        break;
      case "bulkactions":
        if (data.action === "Export") await this.exportTable();
        break;

      default:
        break;
    }
  };

  async dataSourceListener(event) {
    console.log(event.data);

    switch (event.action) {
      case "filter":
        this.dataSource = event.data;
        this.dataTable.dataChangedObs.next(true);
        break;

      case "createFilter":
        await this.createCustomFilterAsync(event.data);
        await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "updateFilter":
        await this.updateCustomFilterAsync(event.data);
        await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "processFilter":
        await this.processCustomFilterAsync(event.data);
        this.dataTable.dataChangedObs.next(true);
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "deleteFilter":
        await this.deleteCustomFilterByIdAsync(event.data);
        await this.getAllCustomFiltersAsync();
        this.dataFilter.dataChangedObs.next(true);
        break;

      case "clear":
        // Loader here
        await this.initializeLicenses();
        break;

      default:
        break;
    }
  }

  // Get All Licenses Async
  private async getLicensesAsync() {
    return await this.licenseService.getLicenses().toPromise();
  }

  // Initialize Licenses
  async initializeLicenses() {
    try {
      this.loader.main.showSpinner = true;
      this.dataSource = await this.getLicensesAsync();
      this.dataTable.dataChangedObs.next(true);
      this.dataFilter.dataChangedObs.next(true);
    } catch (error) {
      this.loader.main.type = "errorCard";
      this.loader.main.status = "We couldn't load the data.";
    } finally {
      this.loader.main.showSpinner = false;
    }
  }

  onModalDelete(id: number) {
    this.showModalSpinner = true;
    this.licenseService.getLicenseById(id).subscribe((data: any) => {
      this.singleLicense = data;

      setTimeout(() => {
        this.showModalSpinner = false;
      }, 1000);
    });
  }

  onDelete(id: number) {
    this.isLoading = true;
    this.processDelete(id);

    //   setTimeout(() => {
    //     this.isLoading = false;
    //   }, 2000);
  }

  processDelete(id: number) {
    this.licenseService.deleteLicense(id).subscribe((res) => {
      this.showModalSuccess = !this.showModalSuccess;
    });
  }

  refresh() {
    this.showSpinner = true;
    this.showModalSuccess = !this.showModalSuccess;
    this.isLoading = !this.isLoading;
    this.ngOnInit();
  }

  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Fetching data...";

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }

  async exportTable() {
    this.loader.export = true;
    const exportName = "Export License - " + Date.now();
    const columns = [
      { title: "Id", value: "id" },
      { title: "License name", value: "name" },
      { title: "Maximum users", value: "maxUsers" },
      { title: "Monthly price", value: "monthlyPrice" },
      { title: "Quarterly price", value: "quarterlyPrice" },
      { title: "Annual price", value: "annuallyPrice" },
      { title: "Max storage", value: "maxStorage" },
      { title: "Creation Date", value: "createdDate" },
    ];

    const dataSource: any = await this.getLicensesAsync();

    dataSource.forEach((d) => {
      d.dueDate = this.datepipe.transform(d.dueDate, "dd/MM/yyyy h:m:s");
      d.createdDate = this.datepipe.transform(
        d.createdDate,
        "dd/MM/yyyy h:m:s"
      );
      if (d.name === undefined || d.name === null) d.name = "";
      if (d.maxUsers === undefined || d.maxUsers === null) d.maxUsers = "";
      if (d.monthlyPrice === undefined || d.monthlyPrice === null)
        d.monthlyPrice = "";
      if (d.quarterlyPrice === undefined || d.quarterlyPrice === null)
        d.quarterlyPrice = "";
      if (d.annuallyPrice === undefined || d.annuallyPrice === null)
        d.annuallyPrice = "";
      if (d.maxStorage === undefined || d.maxStorage === null)
        d.maxStorage = "";
      return d;
    });

    exportTableToCSV(dataSource, columns, exportName);
    this.loader.export = false;
  }

  // ALL FILTERS

  async getAllCustomFiltersAsync() {
    try {
      let data = await this.licenseService.getGroupCustomFilters().toPromise();
      console.log(data, "filters");
      this.customFilters = data;
    } catch (error) {
      let msg = "Error fetching filters. Please try again!";
      msg = error.error.message ? error.error.message : msg;
      this.toastr.error(msg, "Filter Error");
      console.log(error, "filters error");
    }
  }

  async createCustomFilterAsync(data: any) {
    try {
      await this.licenseService.newGroupCustomFilter(data).toPromise();
      // this.sharedSvc.sweetAlertSucess("Filter created successfully!");
    } catch (error) {
      let msg = "Error creating filter. Please try again!";
      msg = error.error.message ? error.error.message : msg;
      this.toastr.error(msg, "Filter Error");
      console.log(error, "filters error");
    }
  }

  async updateCustomFilterAsync(data: any) {
    try {
      await this.licenseService.updateGroupCustomFilter(data).toPromise();
      // this.sharedSvc.sweetAlertSucess("Filter updated successfully!");
    } catch (error) {
      let msg = "Error creating filter. Please try again!";
      msg = error.error.message ? error.error.message : msg;
      this.toastr.error(msg, "Filter Error");
      console.log(error, "filters error");
    }
  }

  async processCustomFilterAsync(filter: any) {
    try {
      console.log(filter, "filter");
      let data: any = await this.licenseService
        .getFilteredGroups(filter.noOfTickets, filter.noOfTicketsResolved)
        .toPromise();
      this.dataSource = data;
      console.log(data, "data");
      // this.sharedSvc.sweetAlertSucess("Filter processed successfully!");
    } catch (error) {
      let msg = "Error filtering data. Please clear to try again!";
      msg = error.error.message ? error.error.message : msg;
      this.toastr.error(msg, "Filter Error");
    }
  }

  async deleteCustomFilterByIdAsync(id) {
    try {
      await this.licenseService.deleteGroupCustomFilter(id).toPromise();
      // this.sharedSvc.sweetAlertSucess("Filter deleted successfully!");
    } catch (error) {
      console.log(error, "filters error");
    }
  }
}
