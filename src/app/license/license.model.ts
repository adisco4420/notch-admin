// license.model.ts

export interface License {
  annuallyPrice: Number;
  annuallyPriceUSD: Number;
  createdDate: String;
  id: Number;
  maxStorage: Number;
  maxUsers: String;
  modules: String[];
  monthlyPrice: Number;
  monthlyPriceUSD: Number;
  name: String;
  quarterlyPrice: Number;
  descrip: string;
}
