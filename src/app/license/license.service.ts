import { retry, catchError, map } from "rxjs/operators";
import { AppService } from "./../app.service";
import { Injectable } from "@angular/core";
import { License } from "./license.model";
import { HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LicenseService {
  url: string;

  constructor(private appService: AppService) {}

  getLicenses() {
    this.url = `${this.appService.baseURL}/getLicenses/`;
    return this.appService
      .get(this.url)
      .pipe(retry(1), catchError(this.handleError));
  }

  getLicenseById(id: number) {
    this.url = `${this.appService.baseURL}/${id}/getLicense/`;
    return this.appService
      .get(this.url)
      .pipe(retry(1), catchError(this.handleError));
  }

  newLicense(license: License) {
    this.url = `${this.appService.baseURL}/newLicense/`;
    return this.appService.post(this.url, license);
  }

  editLicense(license: License) {
    this.url = `${this.appService.baseURL}/editLicense/`;
    return this.appService.post(this.url, license);
  }

  deleteLicense(id: number) {
    this.url = `${this.appService.baseURL}/${id}/deleteLicense/`;
    return this.appService.post(this.url, id);
  }

  // Filters

  // Group Custom Filter
  getGroupCustomFilters() {
    this.url = `${this.appService.baseURL}/getGroupCustomFilters/`;
    return this.appService.get(this.url);
    // return this.http.get(
    //   this.signupApi +
    //     `/${this.gs.orgID}/${this.gs.user.id}/getGroupCustomFilters`
    // );
  }

  fetchAgentGroups(agentID) {
    this.url = `${this.appService.baseURL}/getGroupCustomFilters/`;
    return this.appService.get(this.url);
    // return this.http.get(
    //   `${this.signupApi}/getAgentGroups/${agentID}/${this.gs.org.id}`
    // );
  }

  newGroupCustomFilter(payload) {
    this.url = `${this.appService.baseURL}/getGroupCustomFilters/`;
    return this.appService.get(this.url);
    // payload = { ...payload, orgID: this.gs.orgID, userID: this.gs.user.id };
    // return this.http.post(this.signupApi + `/newGroupCustomFilter`, payload);
  }

  updateGroupCustomFilter(payload) {
    this.url = `${this.appService.baseURL}/getGroupCustomFilters/`;
    return this.appService.get(this.url);
    // payload = { ...payload, orgID: this.gs.orgID, userID: this.gs.user.id };
    // return this.http.post(this.signupApi + `/updateGroupCustomFilter`, payload);
  }

  getFilteredGroups(ticketValue, resolvedValue) {
    this.url = `${this.appService.baseURL}/getGroupCustomFilters/`;
    return this.appService.get(this.url);
    // return this.http.get(
    //   this.signupApi +
    //     `/${ticketValue}/${resolvedValue}/${this.gs.orgID}/getFilteredGroupsByNoOfTicketsAndNoOfTicketResolved`
    // );
  }

  deleteGroupCustomFilter(id) {
    this.url = `${this.appService.baseURL}/${id}/deleteGroupCustomFilter/`;
    return this.appService.delete(this.url);
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
    }),
  };

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
