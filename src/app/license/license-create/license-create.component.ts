import { LicenseService } from "./../license.service";
import { Component, OnInit, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  FormArray,
  ValidatorFn,
} from "@angular/forms";
import { Router } from "@angular/router";
import { modulesTable } from "src/app/_shared/modules";
import { SharedService } from "src/app/_shared/shared.service";
import { ToastrService } from "ngx-toastr";
import { REMOVESPACESONLY } from "src/app/_shared/utils/utils";
import { throwError } from "rxjs";

@Component({
  selector: "app-license-create",
  templateUrl: "./license-create.component.html",
  styleUrls: ["./license-create.component.css"],
})
export class LicenseCreateComponent implements OnInit {
  vm: any = {
    isFound: false,
    status: "Checking license name...",
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  isLoading: boolean = false;
  licenseForm: FormGroup;
  arrayModules: any;
  arrayLicenses: any = [];
  numberOfModules = 0;
  isTrialPeriod: boolean = false;

  constructor(
    private fb: FormBuilder,
    private licenseService: LicenseService,
    private sharedService: SharedService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Fetching data...";
  }

  async ngOnInit() {
    await this.ngOnLoad();
    this.createForm();
    this.filterLicenseByName();
  }

  // ngOnLoad
  async ngOnLoad() {
    try {
      this.arrayModules = await this.getModulesAsync();
      this.arrayLicenses = await this.getLicensesAsync();
    } catch (error) {
      throwError(error);
    } finally {
      this.showSpinner = false;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.licenseForm.controls;
  }

  createForm() {
    this.licenseForm = this.fb.group({
      name: ["", [Validators.required, REMOVESPACESONLY]],
      descrip: ["", [Validators.required, REMOVESPACESONLY]],
      maxUsers: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      monthlyPrice: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      monthlyPriceUSD: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      quarterlyPrice: [
        1,
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      quarterlyPriceUSD: [
        1,
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      annuallyPrice: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      annuallyPriceUSD: [
        "",
        [Validators.required, Validators.pattern("^[0-9]*$")],
      ],
      maxStorage: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
      modules: new FormArray([], this.minSelectedCheckboxes(1)),
      trialPeriod: [0, [Validators.pattern("^[0-9]*$")]],
    });

    this.addCheckboxes();
  }

  // Get All Modules Async
  async getModulesAsync() {
    return await this.sharedService.getSectionModules(0).toPromise();
  }

  // Get All Licenses Async
  private async getLicensesAsync() {
    return await this.licenseService.getLicenses().toPromise();
  }

  // Filter License
  async filterLicenseByName() {
    let name = this.f.name.value;
    // console.log(name, "name");

    this.vm.isFound =
      this.arrayLicenses.filter(
        (e) => e.name.toLowerCase() === name.toLowerCase()
      ).length > 0;
    // console.log(this.vm.isFound, "isFound");
  }

  // getModules() {
  //   this.sharedService
  //     .getSectionModules(0)
  //     .subscribe((data) => {
  //       this.arrayModules = data;
  //     })
  //     .add(() => {
  //       this.createForm();
  //       this.showSpinner = false;
  //     });
  // }

  checkModules() {
    console.log(this.arrayModules, "arrayModules");

    // this.licenseForm.controls.modules.controls

    const selectedModules = this.licenseForm.value.modules
      .map((v, i) => (v ? this.arrayModules[i] : null))
      .filter((v) => v !== null);

    console.log(selectedModules, "selectedModules");
  }

  onSubmit() {
    this.isLoading = true;
    const selectedModules = this.licenseForm.value.modules
      .map((v, i) => (v ? this.arrayModules[i] : null))
      .filter((v) => v !== null);

    this.licenseForm.value.modules = selectedModules;
    this.numberOfModules = selectedModules.length;
    this.processForm();
  }

  processForm() {
    const payload = this.licenseForm.value;
    this.licenseService.newLicense(payload).subscribe(
      (res) => {
        this.router.navigate(["license/list"]);
      },
      (err) => this.toastr.error("Something went wrong")
    );
  }

  private addCheckboxes() {
    this.arrayModules.map((o, i) => {
      // const control = new FormControl(i); // if first item set to true, else false
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.licenseForm.controls.modules as FormArray).push(control);
    });
    console.log(this.arrayModules, "arrayModules");
  }

  private minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map((control) => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => (next ? prev + next : prev), 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  onChangeTrialPeriod() {
    this.isTrialPeriod = !this.isTrialPeriod;
    console.log(this.f.trialPeriod.value, "trialPeriod value");
  }
}
