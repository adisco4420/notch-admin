import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { ReactiveFormsModule, FormBuilder, FormsModule } from "@angular/forms";

import { LicenseRoutingModule } from "./license-routing.module";
import { LicenseListComponent } from "./license-list/license-list.component";
import { LicenseEditComponent } from "./license-edit/license-edit.component";
import { LicenseCreateComponent } from "./license-create/license-create.component";
import { LicenseDetailComponent } from "./license-detail/license-detail.component";
import { LaddaModule } from "angular2-ladda";
import { SharedModule } from "../_shared/shared.module";
import { MentionModule } from "angular-mentions";

@NgModule({
  declarations: [
    LicenseListComponent,
    LicenseEditComponent,
    LicenseCreateComponent,
    LicenseDetailComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LicenseRoutingModule,
    LaddaModule,
    MentionModule,
  ],

  providers: [DatePipe],

  // schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LicenseModule {}
