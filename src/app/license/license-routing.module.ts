import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicenseListComponent } from './license-list/license-list.component';
import { LicenseDetailComponent } from './license-detail/license-detail.component';
import { LicenseCreateComponent } from './license-create/license-create.component';
import { LicenseEditComponent } from './license-edit/license-edit.component';


const routes: Routes = [
  {
    path: 'list',
    component: LicenseListComponent,
  },
  {
    path: 'create-new',
    component: LicenseCreateComponent,
  },
  {
    path: 'edit/:id',
    component: LicenseEditComponent,
  },
  {
    path: 'view/:id',
    component: LicenseDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LicenseRoutingModule { }
