import { Component, OnInit } from '@angular/core';
import { License } from '../license.model';
import { LicenseService } from '../license.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-license-detail',
  templateUrl: './license-detail.component.html',
  styleUrls: ['./license-detail.component.css']
})

export class LicenseDetailComponent implements OnInit {

  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: License;

  constructor(
    private licenseService: LicenseService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Fetching data...";
  }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.getLicenseById(id);
  }

  getLicenseById(id) {
    this.licenseService
      .getLicenseById(id).subscribe((data: any) => {
        console.log(data, 'data by id')
        this.dataSource = data;
        this.showSpinner = false;
      });
  }
}
