import { environment } from "./../environments/environment";
import { map, catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AppService {
  baseURL = `${environment.baseURL}/adminservice`;
  adminServiceURL = `${environment.baseURL}/adminservice`;
  signUpURL = `${environment.baseURL}/signup`;
  requestOptions: any;

  //httpStatus = 'firstload';

  constructor(private http: HttpClient) {}

  get(url: string) {
    return this.http.get(`${url}`);
  }

  post(url: string, payLoad: any) {
    return this.http.post(`${url}`, payLoad);
  }

  put(url: string, payLoad: any) {
    return this.http.put(`${url}`, payLoad);
  }

  delete(url: string) {
    return this.http.delete(`${url}`);
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
