import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  url: string;

  constructor(private appService: AppService) { }

  getUsers() {
    this.url = `${this.appService.signUpURL}/getAllUsers/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.appService.handleError)
      );
  }

  getUserById(id: number) {
    this.url = `${this.appService.signUpURL}/getAdminUser/${id}/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.appService.handleError)
      );
  }

  // deleteEmailTemplate(id: number) {
  //   this.url = `${this.appService.baseURL}/${id}/deleteEmailTemplate/`;
  //   return this.appService.delete(this.url);
  // }
}
