import { UserStatusComponent } from './user-status/user-status.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllUsersComponent } from './all-users/all-users.component';


const routes: Routes = [
  {
    path: 'all',
    component: AllUsersComponent,
  },
  {
    path: 'status/:id',
    component: UserStatusComponent,
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UsersRoutingModule { }
