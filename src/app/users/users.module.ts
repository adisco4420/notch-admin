import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { AllUsersComponent } from './all-users/all-users.component';
import { UserStatusComponent } from './user-status/user-status.component';
import { SharedModule } from '../_shared/shared.module';


@NgModule({
  declarations: [AllUsersComponent, UserStatusComponent],
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
  ]
})
export class UsersModule { }
