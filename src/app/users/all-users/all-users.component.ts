import { BehaviorSubject } from "rxjs";
import { UsersService } from "./../users.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { exportTableToCSV } from "src/app/_shared/utils/utils";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-all-users",
  templateUrl: "./all-users.component.html",
  styleUrls: ["./all-users.component.css"],
})
export class AllUsersComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "User ID", key: "id" },
      {
        title: "Full name",
        key: "name",
        transform: (fieldData, rowData) =>
          rowData.firstName + " " + rowData.lastName,
      },
      { title: "Country", key: "country" },
      {
        title: "Creation date",
        key: "createdDate",
        transform: (fieldData, rowData) =>
          new Date(fieldData).toLocaleDateString(),
      },
      {
        title: "Status",
        key: "planStatus",
        transform: (fieldData, rowData) => "Active",
      },
      { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Email", "Sms", "Export"],
      singleActions: ["View"],
    },
  };
  dataFilter = {
    dataChangedObs: new BehaviorSubject(null),
    accordions: [
      { name: "Country", type: "box", search: true, filterKey: "country" },
      // {
      //   name: "Status",
      //   type: "box",
      //   search: false,
      //   filterKey: "planStatus",
      // },
      {
        name: "Created Date",
        type: "date",
        filterKey: "createdDate",
        icon: "fa fa-calendar",
      },
    ],
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: any = [];
  showModal: boolean;
  modalSpinnerType: string;
  modalSpinnerStatus: string;
  showModalSpinner: boolean;
  isLoading: boolean = false;
  showModalSuccess: boolean = false;

  constructor(
    private usersService: UsersService,
    private datepipe: DatePipe,
    private router: Router
  ) {
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Fetching data...";

    this.modalSpinnerType = "jsBin";
    this.modalSpinnerStatus = "";
  }

  ngOnInit() {
    this.initializeUsers();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "bulkactions":
        if (data.action === "Export") {
          await this.exportTable();
        }
        break;
      case "singleaction":
        if (data.action === "View") {
          this.router.navigate(["/users/status/" + data.data.id]);
        } else if (data.action === "View License") {
          // this.router.navigate(['/tenants/license/' + data.data.id]);
        } else if (data.action === "Delete") {
          // @ts-ignore
          //   document.querySelector("[data-target='#deleteLicenseModal'").click()
          // this.onModalDelete(data.data.id)
        }
        break;

      default:
        break;
    }
  };

  async dataSourceListener(event) {
    console.log(event.data);

    switch (event.action) {
      case "filter":
        this.dataSource = event.data;
        this.dataTable.dataChangedObs.next(true);
        break;

      case "clear":
        // Loader here
        await this.getAllUsersAsync();
        break;

      default:
        break;
    }
  }

  // Get All Users
  private async getAllUsersAsync() {
    return await this.usersService.getUsers().toPromise();
  }

  // Initialize Users
  async initializeUsers() {
    try {
      this.dataSource = await this.getAllUsersAsync();
      console.log(this.dataSource, "dataSource");
      this.dataFilter.dataChangedObs.next(true);
      this.dataTable.dataChangedObs.next(true);
      this.showSpinner = false;
    } catch (error) {
      this.spinnerType = "errorCard";
      this.setSpinnerStatus = "We couldn't load the data.";
    }
  }

  // getUsers() {
  //   this.usersService.getUsers().subscribe(
  //     (data: any) => {
  //       console.log(data, "data");

  //       this.dataSource = data;
  //       this.showSpinner = false;
  //     },
  //     (err) => {
  //       this.spinnerType = "errorCard";
  //       this.setSpinnerStatus = "We couldn't load the data.";
  //     }
  //   );
  // }

  // onModalDelete(id: number) {
  //   this.showModalSpinner = true;
  //   this.usersService
  //     .getUserById(id)
  //     .subscribe((data: any) => {
  //       this.singleEmailTemplate = data;

  //       setTimeout(() => {
  //         this.showModalSpinner = false;
  //       }, 1000);

  //     });
  // }

  // onDelete(id: number) {
  //   this.isLoading = true;
  //   this.processDelete(id);

  //   // setTimeout(() => {
  //   //   this.isLoading = false;
  //   // }, 2000);
  // }

  // processDelete(id: number) {
  //   this.emailTemplateService.deleteEmailTemplate(id).subscribe((res) => {
  //     this.showModalSuccess = !this.showModalSuccess;
  //   });
  // }

  closeModal() {
    this.showSpinner = true;
    this.showModalSuccess = !this.showModalSuccess;
    this.ngOnInit();
  }

  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Retrying...";
    // this.ngOnInit();

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }

  async exportTable() {
    const exportName = "Export Users - " + Date.now();
    const columns = [
      { title: "Id", value: "id" },
      { title: "First Name", value: "firstName" },
      { title: "Last Name", value: "lastName" },
      { title: "Country", value: "country" },
      { title: "Status", value: "planStatus" },
      { title: "Creation Date", value: "createdDate" },
    ];

    const dataSource: any = await this.getAllUsersAsync();

    dataSource.forEach((d) => {
      // d.createdDate = this.datepipe.transform(
      //   d.createdDate,
      //   "dd/MM/yyyy h:m:s"
      // );
      if (d.firstName === undefined || d.firstName === null) d.firstName = "";
      if (d.lastName === undefined || d.lastName === null) d.lastName = "";
      if (d.country === undefined || d.country === null) d.country = "";
      if (d.planStatus === undefined || d.planStatus === null)
        d.planStatus = "";
      return d;
    });

    exportTableToCSV(dataSource, columns, exportName);
  }
}
