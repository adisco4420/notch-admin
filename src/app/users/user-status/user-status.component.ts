import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-status',
  templateUrl: './user-status.component.html',
  styleUrls: ['./user-status.component.css']
})

export class UserStatusComponent implements OnInit {

  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataObject: any;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Loading...";
  }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.getUserById(id);
  }

  getUserById(userId) {
    this.usersService
      .getUserById(userId)
      .subscribe((data: any) => {
        console.log(data, 'userId data');
        this.dataObject = data;
        this.showSpinner = false;
      }, (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load this view.";
      });
  }

  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Retrying...";
    // this.ngOnInit();

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }


}
