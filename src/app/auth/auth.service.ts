import { AdminUserI } from "./../models/admin-user.model";
import { Injectable } from "@angular/core";
import { AppService } from "../app.service";
import { retry, catchError } from "rxjs/operators";
import { License } from "../license/license.model";
import { HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { throwError, BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  url: string;
  private storageKey = "AdminCurrentUser";

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<AdminUserI>;

  constructor(private appService: AppService) {
    this.currentUserSubject = new BehaviorSubject<any>(this.userDetails);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): AdminUserI {
    return this.currentUserSubject.value;
  }
  get userDetails(): AdminUserI {
    return JSON.parse(localStorage.getItem(this.storageKey));
  }
  storeUser(data) {
    localStorage.setItem(this.storageKey, JSON.stringify(data));
    return this.currentUserSubject.next(data);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem(this.storageKey);
    this.currentUserSubject.next(null);
  }

  doLogin(payload) {
    this.url = `${this.appService.baseURL}/doLogin/`;
    return this.appService.post(this.url, payload);
  }
  changePwd(payload) {
    let body = payload;
    if (!payload.email) {
      body = { ...body, email: this.currentUserValue.email };
    }
    this.url = `${this.appService.baseURL}/changePassword`;
    return this.appService.post(this.url, body);
  }

  getLicenseById(id: number) {
    this.url = `${this.appService.baseURL}/${id}/getLicense/`;
    return this.appService.get(this.url);
  }

  newLicense(license: License) {
    this.url = `${this.appService.baseURL}/newLicense/`;
    return this.appService.post(this.url, license);
  }

  editLicense(license: License) {
    this.url = `${this.appService.baseURL}/editLicense/`;
    return this.appService.post(this.url, license);
  }

  deleteLicense(id: number) {
    this.url = `${this.appService.baseURL}/${id}/deleteLicense/`;
    return this.appService.post(this.url, id);
  }

  // Http Options
  // httpOptions = {
  //   headers: new HttpHeaders({
  //     "Content-Type": "application/json",
  //   }),
  // };

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
