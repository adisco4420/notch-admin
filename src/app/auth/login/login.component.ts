import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { VALIDEMAILREGEX } from "src/app/_shared/utils/utils";
import { AppService } from "src/app/app.service";
import { AuthService } from "../auth.service";
import { Route } from "@angular/compiler/src/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  vm: any = {};
  loader: any = {
    btn: {
      login: false,
    },
  };
  loginForm: FormGroup;
  returnUrl: string;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authSvc: AuthService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    // redirect to home if already logged in
    if (this.authSvc.currentUserValue) {
      this.router.navigate(["/license/list"]);
    }
  }

  ngOnInit() {
    // this.loader.btn.login = false;
    this.onCreateForm();
    // get return url from route parameters or default to '/'
    // this.returnUrl =
    //   this.route.snapshot.queryParams["returnUrl"] || "//license/list";
  }

  get f() {
    return this.loginForm.controls;
  }

  // Private
  private onCreateForm() {
    this.loginForm = this.fb.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(VALIDEMAILREGEX),
        ]),
      ],
      password: ["", Validators.required],
    });
  }

  // Handle login
  async onLogin() {
    const payload = this.loginForm.value;
    try {
      this.loader.btn.login = true;
      const res: any = await this.authSvc.doLogin(payload).toPromise();
      const data = { ...res.user.adminUser, role: res.user.roles[0] };
      this.authSvc.storeUser(data);
      this.toastr.success(res.message);
      this.router.navigate(["/license/list"]);
    } catch (err) {
      console.log(err);
      let msg = "Login failed, Please try again!";
      const status =
        err && err.error && err.error.status ? err.error.status : "FAILED";
      if (status === "CONFIRM_EMAIL") {
        this.toastr.warning("Please change your password");
        this.router.navigate(["/settings/change-password"], {
          queryParams: { email: payload.email },
        });
      } else {
        msg = err && err.error && err.error.message ? err.error.message : msg;
        this.toastr.error(msg);
      }
    } finally {
      this.loader.btn.login = false;
      // this.router.navigate([this.returnUrl]);
    }
  }
}
