import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ToastrModule } from "ngx-toastr";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HeaderComponent } from "./_shared/header/header.component";
import { AiComponent } from "./_shared/ai/ai.component";
import { MentionModule } from "angular-mentions";
import { SharedModule } from "./_shared/shared.module";
import { DatePipe } from "@angular/common";
import { NgSelectModule } from "@ng-select/ng-select";

@NgModule({
  declarations: [AppComponent, HeaderComponent, AiComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),

    // LaddaModule,
    NgbModule,
    MentionModule,
    SharedModule,
  ],
  // exports: [MentionModule, LaddaModule, SharedModule, ToastrModule],
  exports: [MentionModule, SharedModule, ToastrModule],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
