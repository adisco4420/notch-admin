export interface AdminUserI {
    "createdDate": string;
    "email": string;
    "emailConfirm": true;
    "firstName": string;
    "id": number;
    "lastName": string;
    "password": string;
    "status": number;
}

