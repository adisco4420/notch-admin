import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowTemplateComponent } from './flow-template.component';

describe('FlowTemplateComponent', () => {
  let component: FlowTemplateComponent;
  let fixture: ComponentFixture<FlowTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
