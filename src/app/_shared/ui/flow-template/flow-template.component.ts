import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewEncapsulation,
} from "@angular/core";
import { SharedService } from "../../shared.service";

@Component({
  selector: "flow-template",
  templateUrl: "./flow-template.component.html",
  styleUrls: ["./flow-template.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class FlowTemplateComponent implements OnInit {
  @Input() id: string;
  @Input() options: any;
  private element: any;

  constructor(private el: ElementRef, private sharedService: SharedService) {
    this.element = el.nativeElement;
  }

  ngOnInit() {
    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }

    // close modal on background click
    // this.element.addEventListener("click", (el) => {
    //   if (el.target.className === "ft-overlay-container") {
    //     this.close();
    //   }
    // });

    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.sharedService.add(this);
  }

  // remove self from modal service when component is destroyed
  ngOnDestroy(): void {
    this.sharedService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(): void {
    let hidden = document.getElementById("ft--hidden");
    if (hidden) document.getElementById("ft--hidden").style.display = "none";
    let modalClass = this.element.querySelector(".ft-overlay-container");
    modalClass.classList.toggle("d-block");
    document.body.classList.add("ft--open");
  }

  // close modal
  close(): void {
    let hidden = document.getElementById("ft--hidden");
    if (hidden) document.getElementById("ft--hidden").style.display = "block";
    // document.getElementById("ft--hidden").style.display = "block";
    let modalClass = this.element.querySelector(".ft-overlay-container");
    modalClass.classList.toggle("d-block");
    document.body.classList.remove("ft--open");
  }

  closeTemplate(id: string) {
    console.log(this.id, "id");

    // close modal specified by id
    this.sharedService.modals.filter((x) => x.id === id)[0];
    this.close();
  }
}
