import { AuthService } from './../../auth/auth.service';
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  activeView;

  constructor(
    public router: Router,
    private authSrv: AuthService) {}

  ngOnInit() {}

  searchToggle() {
    this.activeView ? (this.activeView = false) : (this.activeView = true);
  }
  logout() {
    this.authSrv.logout();
    this.router.navigate(['/login']);
  }
}
