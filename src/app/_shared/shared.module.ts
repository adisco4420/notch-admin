import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoadingSpinnerComponent } from "./ui/loading-spinner/loading-spinner.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DateAgoPipe } from "./pipes/date-ago.pipe";
import { DatatableModule } from "inn-datatable";
import { FlowTemplateComponent } from "./ui/flow-template/flow-template.component";
import { LaddaModule } from "angular2-ladda";
import { DataFilterComponent } from "./ui/data-filter/data-filter.component";
import { UniquePipe } from "./pipes/unique.pipe";
import { FilterPipe } from "./pipes/filter.pipe";
import { NgSelectModule } from "@ng-select/ng-select";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";

@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    DateAgoPipe,
    FilterPipe,
    UniquePipe,
    FlowTemplateComponent,
    DataFilterComponent,
  ],
  imports: [
    CommonModule,
    LaddaModule,
    FormsModule,
    DatatableModule,
    NgSelectModule,
    BsDatepickerModule.forRoot(),
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    LaddaModule,
    NgSelectModule,
    LoadingSpinnerComponent,
    FlowTemplateComponent,
    DateAgoPipe,
    FilterPipe,
    UniquePipe,
    DatatableModule,
    DataFilterComponent,
  ],
})
export class SharedModule {}
