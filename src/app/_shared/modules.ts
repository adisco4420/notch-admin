export const modulesTable = [
    { id: 0, name: 'Client' },
    { id: 1, name: 'Sales' },
    { id: 2, name: 'Teams' },
    { id: 3, name: 'Leads' }
];

export const modulesTableNew = [
    { id: 'Client', name: 'Client' },
    { id: 'Sales', name: 'Sales' },
    { id: 'Teams', name: 'Teams' },
    { id: 'Leads', name: 'Leads' }
];