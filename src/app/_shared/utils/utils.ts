import { AbstractControl, FormGroup } from "@angular/forms";

export const exportTableToCSV = (
  data: any[],
  columns: { value: string; title: string }[],
  filename
) => {
  // Nathan Apis - fix output date
  if (data[0].createdOn) {
    data = data.map((res: any) => {
      if (res.paymentDueDate) {
        res.paymentDueDate = new Date(res.paymentDueDate).toDateString();
      }
      if (res.endDate) {
        res.endDate = new Date(res.endDate).toDateString();
      }
      res.createdOn = new Date(res.createdOn).toDateString();

      return res;
    });
  }

  let newData = [];
  data.forEach((dat) => {
    newData.push(columns.map((col) => `"${dat[col.value]}"`).join(","));
  });
  newData = [columns.map((col) => col.title).join(","), ...newData];
  downloadCSV(newData.join("\n"), `${filename}.csv`);
};

export const downloadCSV = (csv, filename) => {
  var csvFile;
  var downloadLink;
  // CSV file
  csvFile = new Blob([csv], { type: "text/csv" });
  // Download link
  downloadLink = document.createElement("a");

  // File name
  downloadLink.download = filename;

  // Create a link to the file
  downloadLink.href = window.URL.createObjectURL(csvFile);

  // Hide download link
  downloadLink.style.display = "none";

  // Add the link to DOM
  document.body.appendChild(downloadLink);

  // Click download link
  downloadLink.click();
};

// This will not allow only spaces to be entered.
export function REMOVESPACESONLY(control: AbstractControl) {
  if (control && control.value && !control.value.replace(/\s/g, "").length) {
    control.setValue("");
  }
  return null;
}

// custom validator to check that two fields match
export const MustMatch = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};

// tslint:disable-next-line:max-line-length
export const VALIDEMAILREGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

// export function ONLYNUMBERSALLOWED(control: AbstractControl) {
//   if (control && control.value && !control.value.replace(/\s/g, "").length) {
//     control.setValue("");
//   }
//   return null;
// }

export function extractDeepPropertyByMapKey(obj: any, map: string): any {
  const keys = map.split(".");
  const head = keys.shift();

  return keys.reduce((prop: any, key: string) => {
    return !isUndefined(prop) && !isNull(prop) && !isUndefined(prop[key])
      ? prop[key]
      : undefined;
  }, obj[head || ""]);
}

export function isUndefined(value: any) {
  return typeof value === "undefined";
}

export function isNull(value: any) {
  return value === null;
}
