import { Injectable } from "@angular/core";
import { AppService } from "../app.service";
import Swal from "sweetalert2";
import { Subject } from "rxjs";
import { environment } from "src/environments/environment";

declare var PaystackPop: any;

@Injectable({
  providedIn: "root",
})
export class SharedService {
  url: string;
  modals: any[] = [];
  callbackObservable = new Subject();

  constructor(private appService: AppService) {}

  getSectionModules(id: number) {
    this.url = `${this.appService.signUpURL}/${id}/getSectionModules/`;
    return this.appService.get(this.url);
  }

  /* -------------------------------
      SWEETALERT BEGINS
  ----------------------------------*/

  sweetAlertGeneralDelete(title, btnText) {
    // return Swal.fire({
    //   title: `${title}?`,
    //   text: "You won't be able to revert this!",
    //   type: "warning",
    //   showCancelButton: true,
    //   confirmButtonColor: "#3085d6",
    //   cancelButtonColor: "#d33",
    //   confirmButtonText: `Yes, ${btnText}!`,
    // });
  }

  /* -------------------------------
      PAYSTACK METHODS
  ----------------------------------*/

  payWithPaystack(payload) {
    var handler = PaystackPop.setup({
      key: environment.paystackKey,
      email: payload.email,
      amount: payload.amount * 100,
      currency: payload.currency,
      ref: "" + Math.floor(Math.random() * 1000000000 + 1),
      metadata: payload.metadata,
      callback: (response) => {
        this.callbackObservable.next({ payload, response });
      },
      onClose: function () {
        // alert("window closed");
      },
    });
    handler.openIframe();
  }

  paystackVerification(reference) {
    // return this.http.get(
    //   `${this.comms_service}/bulk-sms/verify/paystack/${reference}`
    // );
  }

  /* -------------------------------
      MODAL METHODS BEGINS
  ----------------------------------*/

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter((x) => x.id !== id);
  }

  open(id: string) {
    // open modal specified by id
    let modal: any = this.modals.filter((x) => x.id === id)[0];
    modal.open();
  }

  close(id: string) {
    // close modal specified by id
    let modal: any = this.modals.filter((x) => x.id === id)[0];
    modal.close();
  }
  getErrMsg(error): string {
    const msg =
      error && error.error && error.error.message
        ? error.error.message
        : "Error occured try again:";
    return msg;
  }
}
