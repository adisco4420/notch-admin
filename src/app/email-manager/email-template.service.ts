import { EmailTemplate } from './email-template.model';
import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { retry, catchError } from 'rxjs/operators';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EmailTemplateService {

  url: string;

  constructor(private appService: AppService) { }

  getEmailTemplates() {
    this.url = `${this.appService.baseURL}/getEmailTemplates/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getEmailTemplateById(id: number) {
    this.url = `${this.appService.baseURL}/${id}/getEmailTemplate/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  newEmailTemplate(template: EmailTemplate) {
    this.url = `${this.appService.baseURL}/newEmailTemplate/`;
    return this.appService
      .post(this.url, template).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  editEmailTemplate(template: EmailTemplate) {
    this.url = `${this.appService.baseURL}/editEmailTemplate/`;
    return this.appService.post(this.url, template);
  }

  deleteEmailTemplate(id: number) {
    this.url = `${this.appService.baseURL}/${id}/deleteEmailTemplate/`;
    return this.appService.delete(this.url);
  }


  // Triggers
  getTriggers() {
    this.url = `${this.appService.baseURL}/getTriggers/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // Triggers
  getPlaceholders() {
    this.url = `${this.appService.baseURL}/getPlaceholders/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  newTrigger(template: EmailTemplate) {
    this.url = `${this.appService.baseURL}/newTrigger/`;
    return this.appService
      .post(this.url, template).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  editTrigger(template: EmailTemplate) {
    this.url = `${this.appService.baseURL}/editTrigger/`;
    return this.appService.post(this.url, template);
  }

  deleteTrigger(id: number) {
    this.url = `${this.appService.baseURL}/${id}/deleteTrigger/`;
    return this.appService.delete(this.url);
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
