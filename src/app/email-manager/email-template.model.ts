export interface EmailTemplate {
    content: string
    createdDate: string
    id: number
    msgTrigger: string
    subject: string
}


export interface Placeholder {
    id: number
    name: string
}

export interface Trigger {
    id: number
    name: string
}
