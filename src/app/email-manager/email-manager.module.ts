import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmailManagerRoutingModule } from './email-manager-routing.module';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { EditTemplateComponent } from './edit-template/edit-template.component';
import { TemplateListComponent } from './template-list/template-list.component';
import { TemplateViewComponent } from './template-view/template-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
import { SharedModule } from '../_shared/shared.module';
import { MentionModule } from 'angular-mentions';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [
    CreateTemplateComponent,
    EditTemplateComponent,
    TemplateListComponent,
    TemplateViewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmailManagerRoutingModule,
    SharedModule,
    LaddaModule,
    MentionModule,
    AngularEditorModule,
  ],
})

export class EmailManagerModule { }
