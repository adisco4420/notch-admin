import { TemplateViewComponent } from './template-view/template-view.component';
import { EditTemplateComponent } from './edit-template/edit-template.component';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { TemplateListComponent } from './template-list/template-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'templates',
    component: TemplateListComponent,
  },
  {
    path: 'create-new',
    component: CreateTemplateComponent,
  },
  {
    path: 'edit/:id',
    component: EditTemplateComponent,
  },
  {
    path: 'template/:id',
    component: TemplateViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailManagerRoutingModule { }
