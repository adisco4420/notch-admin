import { EmailTemplateService } from "./../email-template.service";
import { EmailTemplate } from "./../email-template.model";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { DatePipe } from "@angular/common";
import { exportTableToCSV } from "src/app/_shared/utils/utils";

@Component({
  selector: "app-template-list",
  templateUrl: "./template-list.component.html",
  styleUrls: ["./template-list.component.css"],
})
export class TemplateListComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "Trigger", key: "msgTrigger" },
      { title: "Subject", key: "subject" },
      {
        title: "Creation Date",
        key: "createdDate",
        transform: (fieldData, rowData) =>
          new Date(fieldData).toLocaleDateString(),
      },
      { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Export"],
      singleActions: ["Edit"],
    },
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: any = [];
  singleEmailTemplate: EmailTemplate;
  showModal: boolean;
  loader: any = {
    export: false,
  };
  modalSpinnerType: string;
  modalSpinnerStatus: string;
  showModalSpinner: boolean;
  isLoading: boolean = false;
  showModalSuccess: boolean = false;

  constructor(
    private datepipe: DatePipe,
    private emailTemplateService: EmailTemplateService,
    private router: Router
  ) {
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Fetching data...";

    this.modalSpinnerType = "jsBin";
    this.modalSpinnerStatus = "";
  }

  ngOnInit() {
    this.initializeEmailTemplates();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "singleaction":
        if (data.action === "Edit")
          this.router.navigate(["/email-manager/edit/" + data.data.id]);
        break;
      case "bulkactions":
        if (data.action === "Export") await this.exportTable();
        break;

      default:
        break;
    }
  };

  // Get All Billings Async
  private async getEmailTemplatesAsync() {
    return await this.emailTemplateService.getEmailTemplates().toPromise();
  }

  // Initialize Billings
  async initializeEmailTemplates() {
    try {
      this.dataSource = await this.getEmailTemplatesAsync();
      console.log(this.dataSource, "dataSource");
      this.dataTable.dataChangedObs.next(true);
      this.showSpinner = false;
    } catch (error) {
      this.spinnerType = "errorCard";
      this.setSpinnerStatus = "We couldn't load the data.";
    }
  }

  onModalDelete(id: number) {
    this.showModalSpinner = true;
    this.emailTemplateService
      .getEmailTemplateById(id)
      .subscribe((data: any) => {
        this.singleEmailTemplate = data;

        setTimeout(() => {
          this.showModalSpinner = false;
        }, 1000);
      });
  }

  onDelete(id: number) {
    this.isLoading = true;
    this.processDelete(id);

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 2000);
  }

  processDelete(id: number) {
    this.emailTemplateService.deleteEmailTemplate(id).subscribe((res) => {
      this.showModalSuccess = !this.showModalSuccess;
    });
  }

  closeModal() {
    this.showSpinner = true;
    this.showModalSuccess = !this.showModalSuccess;
    this.isLoading = !this.isLoading;
    this.ngOnInit();
  }

  retry(spinnerType) {
    console.log(spinnerType, "...from list");
    this.showSpinner = true;
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Retrying...";
    // this.ngOnInit();

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }

  async exportTable() {
    this.loader.export = true;
    const exportName = "Export Template - " + Date.now();
    const columns = [
      { title: "Id", value: "id" },
      { title: "Trigger", value: "msgTrigger" },
      { title: "Subject", value: "subject" },
      { title: "Creation Date", value: "createdDate" },
    ];

    const dataSource: any = await this.getEmailTemplatesAsync();

    dataSource.forEach((d) => {
      d.createdDate = this.datepipe.transform(
        d.createdDate,
        "dd/MM/yyyy h:m:s"
      );
      if (d.msgTrigger === undefined || d.msgTrigger === null)
        d.msgTrigger = "";
      if (d.subject === undefined || d.subject === null) d.subject = "";
      return d;
    });

    exportTableToCSV(dataSource, columns, exportName);
    this.loader.export = false;
  }
}
