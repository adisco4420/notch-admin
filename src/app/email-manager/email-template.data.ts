
export const getSampleTriggers = [
    {
        id: 0,
        name: 'General Welcome Template',
    },
    {
        id: 1,
        name: 'Newsletter Welcome Template',
    },
    {
        id: 2,
        name: 'Personal Outreach Welcome Template',
    },
    {
        id: 3,
        name: 'Invitation Welcome Template',
    }
];

