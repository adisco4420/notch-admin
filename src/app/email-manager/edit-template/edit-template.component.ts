import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { EmailTemplateService } from "../email-template.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { EmailTemplate } from "../email-template.model";
import { getSampleTriggers } from "../email-template.data";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-edit-template",
  templateUrl: "./edit-template.component.html",
  styleUrls: ["./edit-template.component.css"],
})
export class EditTemplateComponent implements OnInit {
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  isLoading: boolean = false;

  createTemplateForm: FormGroup;
  arrayTriggers: any;
  mentionConfig: any;
  templateId: string;
  templateData: any = [];
  editorConfig: AngularEditorConfig;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private emailTemplateService: EmailTemplateService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // Loader settings
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Fetching data...";

    // Get Email Template Id
    this.route.params.subscribe((params) => {
      this.templateId = params["id"];
      if (this.templateId === null)
        this.router.navigate(["email-manager/templates"]);
    });

    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: "10rem",
      minHeight: "auto",
      maxHeight: "auto",
      width: "auto",
      minWidth: "0",
      translate: "yes",
      enableToolbar: true,
      showToolbar: true,
      placeholder: "Enter text here...",
      defaultParagraphSeparator: "",
      defaultFontName: "",
      defaultFontSize: "",
      fonts: [
        { class: "arial", name: "Arial" },
        { class: "times-new-roman", name: "Times New Roman" },
        { class: "calibri", name: "Calibri" },
        { class: "comic-sans-ms", name: "Comic Sans MS" },
      ],
      sanitize: true,
      toolbarPosition: "top",
    };
  }

  async ngOnInit() {
    this.createForm();
    await this.getEmailTemplateByIdAsync(this.templateId);
    const res: boolean = await this.ngOnLoad();
    // Show spinner (with error) if ngOnLoad is false
    // else: stop spinner
    if (res) this.showSpinner = false;
  }

  // Create Form
  createForm() {
    this.createTemplateForm = this.fb.group({
      msgTrigger: ["", Validators.required],
      subject: ["", Validators.required],
      content: ["", Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createTemplateForm.controls;
  }

  //
  async ngOnLoad() {
    try {
      this.arrayTriggers = await this.getTriggersAsync();
      // console.log(this.arrayTriggers, "arrayTriggers");
      let arrayPlaceholders: any = await this.getPlaceholdersAsync();
      // console.log(arrayPlaceholders, "arrayPlaceholders");
      await this.setPlaceholders(arrayPlaceholders);
      return true;
    } catch (error) {
      this.spinnerType = "errorCard";
      this.setSpinnerStatus = "We couldn't load the data.";
      this.toastr.error("Something went wrong. Please reload to try again");
      return false;
    }
  }

  // Get Email Templates By Id - Async
  private async getEmailTemplateByIdAsync(id) {
    try {
      let obj = await this.emailTemplateService
        .getEmailTemplateById(id)
        .toPromise();
      if (obj === null) return;
      this.setFormValues(obj);
      this.templateData = obj;
    } catch (error) {
      this.toastr.error(
        "Something went wrong. Please reload this page to try again"
      );
    }
  }

  // Get All Triggers Async
  private async getTriggersAsync() {
    return await this.emailTemplateService.getTriggers().toPromise();
  }

  // Get All PlaceHolders Async
  private async getPlaceholdersAsync() {
    return await this.emailTemplateService.getPlaceholders().toPromise();
  }

  // Set PlaceHolders
  setPlaceholders(arrayPlaceholders: any) {
    arrayPlaceholders = arrayPlaceholders.map((item) => {
      return item.name;
    });
    this.setMentionConfig(arrayPlaceholders);
  }

  // Initialize Mention Configuration
  setMentionConfig(placeholders) {
    this.mentionConfig = {
      mentions: [
        {
          items: placeholders,
          triggerChar: "@",
          // maxItems: 10,
        },
      ],
    };
  }

  // Set Form Values
  setFormValues(data) {
    this.createTemplateForm.patchValue({
      msgTrigger: data.msgTrigger,
      subject: data.subject,
      content: data.content,
    });
  }

  onModify() {
    const payload = this.createTemplateForm.value;
    this.processForm(payload);
  }

  processForm(payload) {
    this.isLoading = true;
    let newPayload = payload;
    newPayload = { ...payload, id: this.templateData.id };
    this.emailTemplateService.editEmailTemplate(newPayload).subscribe(
      (res) => {
        this.toastr.success("Template updated successfully.", "Success!");
        this.router.navigate(["email-manager/templates"]);
      },
      (err) =>
        this.toastr.error(
          "Error updating email template. Please try again!",
          "Update Failed!"
        ),
      () => (this.isLoading = false)
    );
  }
}
