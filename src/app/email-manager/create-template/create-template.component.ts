import { getSampleTriggers } from "./../email-template.data";
import { EmailTemplateService } from "./../email-template.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { isNgTemplate } from "@angular/compiler";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-create-template",
  templateUrl: "./create-template.component.html",
  styleUrls: ["./create-template.component.css"],
})
export class CreateTemplateComponent implements OnInit {
  mentionConfig: any;
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean;
  isLoading: boolean = false;
  createTemplateForm: FormGroup;
  arrayTriggers: any = [];
  editorConfig: AngularEditorConfig;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private emailTemplateService: EmailTemplateService,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "Fetching data...";
    this.showSpinner = true;
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: "10rem",
      minHeight: "auto",
      maxHeight: "auto",
      width: "auto",
      minWidth: "0",
      translate: "yes",
      enableToolbar: true,
      showToolbar: true,
      placeholder: "Enter text here...",
      defaultParagraphSeparator: "",
      defaultFontName: "",
      defaultFontSize: "",
      fonts: [
        { class: "arial", name: "Arial" },
        { class: "times-new-roman", name: "Times New Roman" },
        { class: "calibri", name: "Calibri" },
        { class: "comic-sans-ms", name: "Comic Sans MS" },
      ],
      sanitize: true,
      toolbarPosition: "top",
    };
  }

  async ngOnInit() {
    this.createForm();
    const res: boolean = await this.ngOnLoad();
    // Show spinner (with error) if ngOnLoad is false
    // else: stop spinner
    if (res) this.showSpinner = false;
  }

  //
  async ngOnLoad() {
    try {
      this.arrayTriggers = await this.getTriggersAsync();
      console.log(this.arrayTriggers, "arrayTriggers");
      let arrayPlaceholders: any = await this.getPlaceholdersAsync();
      console.log(arrayPlaceholders, "arrayPlaceholders");
      await this.setPlaceholders(arrayPlaceholders);
      return true;
    } catch (error) {
      this.spinnerType = "errorCard";
      this.setSpinnerStatus = "We couldn't load the data.";
      this.toastr.error("Something went wrong. Please reload to try again");
      return false;
    }
  }

  createForm() {
    this.createTemplateForm = this.fb.group({
      msgTrigger: ["", Validators.required],
      subject: ["", Validators.required],
      content: ["", Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createTemplateForm.controls;
  }

  // Get All Triggers Async
  private async getTriggersAsync() {
    return await this.emailTemplateService.getTriggers().toPromise();
  }

  // Get All PlaceHolders Async
  private async getPlaceholdersAsync() {
    return await this.emailTemplateService.getPlaceholders().toPromise();
  }

  // Set PlaceHolders
  setPlaceholders(arrayPlaceholders: any) {
    arrayPlaceholders = arrayPlaceholders.map((item) => {
      return item.name;
    });
    // console.log(arrayPlaceholders, 'arrayPlaceholders');
    this.setMentionConfig(arrayPlaceholders);
  }

  // Initialize Mention Configuration
  setMentionConfig(placeholders) {
    this.mentionConfig = {
      mentions: [
        {
          items: placeholders,
          triggerChar: "@",
          // maxItems: 10,
        },
      ],
    };
  }

  onSubmit() {
    this.isLoading = true;
    const payload = this.createTemplateForm.value;
    this.processForm(payload);

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 2000);
  }

  processForm(payload) {
    this.emailTemplateService.newEmailTemplate(payload).subscribe((res) => {
      this.router.navigate(["email-manager/templates"]);
    });
  }
}
