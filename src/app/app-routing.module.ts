import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { AuthGuard } from "./auth/auth.guard";

const appRoutes: Routes = [
  // { path: "", redirectTo: "login", pathMatch: "full" },
  // { path: "", redirectTo: "license/list", pathMatch: "full" },

  {
    path: "login",
    loadChildren: "./auth/auth.module#AuthModule",
    data: { showHeader: false, showSidebar: false },
  },
  {
    path: "license",
    loadChildren: "./license/license.module#LicenseModule",
    canActivate: [AuthGuard],
  },
  {
    path: "email-manager",
    loadChildren: "./email-manager/email-manager.module#EmailManagerModule",
    canActivate: [AuthGuard],
  },
  {
    path: "billing-invoice",
    loadChildren:
      "./billing-invoice/billing-invoice.module#BillingInvoiceModule",
    canActivate: [AuthGuard],
  },
  {
    path: "tenants",
    loadChildren: "./tenants/tenants.module#TenantsModule",
    canActivate: [AuthGuard],
  },
  {
    path: "users",
    loadChildren: "./users/users.module#UsersModule",
    canActivate: [AuthGuard],
  },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsModule",
    // canActivate: [AuthGuard],
  },
  { path: "**", redirectTo: "license/list", pathMatch: "full" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      enableTracing: false, // <-- debugging purposes only
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
