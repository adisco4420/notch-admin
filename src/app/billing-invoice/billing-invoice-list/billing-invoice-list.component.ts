import { BehaviorSubject } from "rxjs";
import { BillingInvoiceService } from "./../billing-invoice.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { exportTableToCSV } from "src/app/_shared/utils/utils";
import { DatePipe } from "@angular/common";
import { SharedService } from "src/app/_shared/shared.service";

@Component({
  selector: "app-billing-invoice-list",
  templateUrl: "./billing-invoice-list.component.html",
  styleUrls: ["./billing-invoice-list.component.css"],
})
export class BillingInvoiceListComponent implements OnInit {
  dataTable = {
    dataChangedObs: new BehaviorSubject(null),
    heads: [
      { title: "checkbox", key: "checkbox" },
      { title: "License name", key: "license" },
      { title: "Client Name", key: "tenantName" },
      {
        title: "Invoice Amount",
        key: "amount",
        transform: (fieldData, rowData) =>
          (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
      },
      {
        title: "Balance Due",
        key: "amount",
        transform: (fieldData, rowData) =>
          (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
      },
      {
        title: "Due Date",
        key: "dueDate",
        transform: (fieldData, rowData) =>
          new Date(fieldData).toLocaleDateString(),
      },
      {
        title: "Creation Date",
        key: "createdDate",
        transform: (fieldData, rowData) =>
          new Date(fieldData).toLocaleDateString(),
      },
      { title: "Action", key: "action" },
    ],
    options: {
      bulkActions: ["Export"],
      singleActions: ["View/Edit", "View License"],
    },
  };
  flow: any = {
    currentId: 0,
    billing: {
      options: {
        title: "Billing Details",
        type: "detail",
      },
      tenantId: 0,
      display: false,
    },
  };
  display: any = {
    showSpinner: false,
    dashboard: false,
    details: false,
    score: false,
  };
  isOpened: boolean = false;
  navMenu: any = {
    default: "dashboard",
    list: [
      { name: "dashboard", selected: true },
      { name: "details", selected: false },
      { name: "score", selected: false },
    ],
  };
  loader: any = {
    dataless: {
      title: "No Data Available!",
      subTitle: "Please create a contact and try again",
      action: "Create Contact",
      success: true,
      exists: true,
    },
    main: {
      default: "sharp",
      spinnerType: "",
      spinnerStyle: { marginTop: "15%" },
      showSpinner: false,
    },
    flow: {
      type: "sharp",
      status: "Loading...",
      styles: { marginTop: "15%" },
      showSpinner: false,
    },
    content: {
      default: "sharp",
      spinnerType: "",
      spinnerStyle: { marginTop: "15%" },
      showSpinner: false,
    },
    export: false,
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  dataSource: any = [];
  dataObject: any;
  showModal: boolean;
  mySubscription: any;
  modalSpinnerType: string;
  modalSpinnerStatus: string;
  showModalSpinner: boolean;
  isLoading: boolean = false;
  showModalSuccess: boolean = false;

  constructor(
    private datepipe: DatePipe,
    private sharedService: SharedService,
    private billingInvoiceService: BillingInvoiceService,
    private router: Router
  ) {
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Fetching data...";

    this.modalSpinnerType = "jsBin";
    this.modalSpinnerStatus = "";
  }

  async ngOnInit() {
    await this.initializeBillings();
  }

  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "bulkactions":
        if (data.action === "Export") await this.exportTable();
        break;
      case "singleaction":
        if (data.action === "View/Edit") {
          this.router.navigate(["/billing-invoice/" + data.data.id]);
        } else if (data.action === "View License") {
          this.router.navigate(["/billing-invoice/status/" + data.data.id]);
        } else if (data.action === "Delete") {
          // @ts-ignore
          document.querySelector("[data-target='#deleteModal'").click();
          this.onModalDelete(data.data.id);
        }
        break;

      default:
        break;
    }
  };

  // Get All Billings
  private async getBillingsAsync() {
    return await this.billingInvoiceService.getBillings().toPromise();
  }

  // Initialize Billings
  async initializeBillings() {
    try {
      this.dataSource = await this.getBillingsAsync();
      console.log(this.dataSource, "dataSource ");
      this.dataTable.dataChangedObs.next(true);
      this.showSpinner = false;
    } catch (error) {
      this.spinnerType = "errorCard";
      this.setSpinnerStatus = "We couldn't load the data.";
    }
  }

  openTemplate(id: string) {
    console.log(id, "id");
    this.sharedService.open(id);
    this.loader.flow.showSpinner = true;
  }

  closeTemplate(id: string) {
    this.sharedService.close(id);
  }

  onModalDelete(id: number) {
    this.showModalSpinner = true;
    this.billingInvoiceService.getBillingById(id).subscribe((data: any) => {
      this.dataObject = data;

      setTimeout(() => {
        this.showModalSpinner = false;
      }, 1000);
    });
  }

  onDelete(id: number) {
    this.isLoading = true;
    this.processDelete(id);
  }

  processDelete(id: number) {
    this.billingInvoiceService.deleteBilling(id).subscribe((res) => {
      this.showModalSuccess = !this.showModalSuccess;
    });
  }

  refresh() {
    this.showSpinner = true;
    this.showModalSuccess = !this.showModalSuccess;
    this.isLoading = !this.isLoading;
    this.ngOnInit();
  }

  retry(spinnerType) {
    this.showSpinner = true;
    this.spinnerType = "wave";
    this.setSpinnerStatus = "Fetching data...";

    setTimeout(() => {
      this.ngOnInit();
    }, 5000);
  }

  onMouseMenu() {
    this.isOpened = !this.isOpened;
  }

  //Filter Select Menu
  async onSelectMenu(menuName) {
    this.navMenu.default = menuName;
    this.navMenu.list.filter(async (m) => {
      m.selected = false;
      if (m.name === menuName) m.selected = true;
      await this.toggleView(menuName);
    });
    // console.log(this.contactMenus, "contactMenus");
  }

  private async toggleView(view) {
    this.loader.content.showSpinner = true;
    // await this.initializeContactById(this.contactId);

    switch (view) {
      case "dashboard":
        this.display.dashboard = true;
        this.display.details = false;
        this.display.score = false;
        break;

      case "details":
        this.display.dashboard = false;
        this.display.details = true;
        this.display.score = false;
        break;

      case "score":
        document.getElementById("open-score-modal").click();
        break;

      default:
        this.display.dashboard = false;
        this.display.details = false;
        this.display.score = false;
        this.loader.spinnerType = "errorCard";
        break;
    }
    setTimeout(async () => (this.loader.content.showSpinner = false), 1000);
  }

  async exportTable() {
    this.loader.export = true;
    const exportName = "Export Billing Invoice - " + Date.now();
    const columns = [
      { title: "Id", value: "id" },
      { title: "Client", value: "tenantName" },
      { title: "License", value: "license" },
      { title: "Invoice", value: "amount" },
      { title: "Balance Due", value: "amount" },
      { title: "Due Date", value: "dueDate" },
      { title: "Creation Date", value: "createdDate" },
    ];

    const dataSource: any = await this.getBillingsAsync();

    dataSource.forEach((d) => {
      d.dueDate = this.datepipe.transform(d.dueDate, "dd/MM/yyyy h:m:s");
      d.createdDate = this.datepipe.transform(
        d.createdDate,
        "dd/MM/yyyy h:m:s"
      );
      if (d.tenantName === undefined || d.tenantName === null)
        d.tenantName = "";
      if (d.license === undefined || d.license === null) d.license = "";
      if (d.amount === undefined || d.amount === null) d.amount = "";
      return d;
    });

    exportTableToCSV(dataSource, columns, exportName);
    this.loader.export = false;
  }
}
