export interface Billing {
    amount: number;
    createdDate: string;
    currency: string;
    descrip: string;
    dueDate: string;
    id: number;
    license: string;
    licenseID: string;
    paymentMethod: string;
    quantity: number;
    tax: string;
    taxAmount: number;
    tenantID: string;
    tenantName: string;
    totalAmount: number;
    unitPrice: number;
}