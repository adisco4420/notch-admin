import { BillingInvoiceRoutingModule } from "./billing-invoice-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { BillingInvoiceListComponent } from "./billing-invoice-list/billing-invoice-list.component";
import { CreateInvoiceBillComponent } from "./create-invoice-bill/create-invoice-bill.component";
import { EditInvoiceBillComponent } from "./edit-invoice-bill/edit-invoice-bill.component";
import { LicenseStatusComponent } from "./license-status/license-status.component";
import { SharedModule } from "../_shared/shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LaddaModule } from "angular2-ladda";
import { BillingDetailsComponent } from "./billing-details/billing-details.component";
import { BillingTransactionsComponent } from "./billing-details/billing-transactions/billing-transactions.component";

@NgModule({
  declarations: [
    BillingInvoiceListComponent,
    CreateInvoiceBillComponent,
    EditInvoiceBillComponent,
    LicenseStatusComponent,
    BillingDetailsComponent,
    BillingTransactionsComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LaddaModule,
    SharedModule,
    BillingInvoiceRoutingModule,
  ],

  exports: [
    BillingInvoiceListComponent,
    CreateInvoiceBillComponent,
    EditInvoiceBillComponent,
    LicenseStatusComponent,
  ],

  providers: [DatePipe],
})
export class BillingInvoiceModule {}
