import { Component, OnInit, AfterViewInit } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { BillingInvoiceService } from "../billing-invoice.service";
import { ToastrService } from "ngx-toastr";
import { LicenseService } from "src/app/license/license.service";
import { TenantsService } from "src/app/tenants/tenants.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { SharedService } from "src/app/_shared/shared.service";
import { SettingsService } from "src/app/settings/settings.service";
import { AuthService } from "src/app/auth/auth.service";

@Component({
  selector: "app-billing-details",
  templateUrl: "./billing-details.component.html",
  styleUrls: ["./billing-details.component.css"],
})
export class BillingDetailsComponent implements OnInit, AfterViewInit {
  dataTable = {
    dashboard: {
      dataChangedObs: new BehaviorSubject(null),
      heads: [
        { title: "checkbox", key: "checkbox" },
        { title: "License name", key: "license" },
        { title: "Client Name", key: "tenantName" },
        {
          title: "Invoice Amount",
          key: "amount",
          transform: (fieldData, rowData) =>
            (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        },
        {
          title: "Balance Due",
          key: "amount",
          transform: (fieldData, rowData) =>
            (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        },
        {
          title: "Due Date",
          key: "dueDate",
          transform: (fieldData, rowData) =>
            new Date(fieldData).toLocaleDateString(),
        },
        {
          title: "Creation Date",
          key: "createdDate",
          transform: (fieldData, rowData) =>
            new Date(fieldData).toLocaleDateString(),
        },
        { title: "Action", key: "action" },
      ],
      options: {
        bulkActions: ["Export"],
        singleActions: ["View/Edit", "View License"],
      },
    },
    transaction: {
      dataChangedObs: new BehaviorSubject(null),
      heads: [
        { title: "checkbox", key: "checkbox" },
        { title: "Id", key: "reference" },
        {
          title: "Amount",
          key: "amount",
          transform: (fieldData, rowData) =>
            (fieldData || "0").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        },
        { title: "Method", key: "whatMode" },
        { title: "Bank", key: "bank" },
        { title: "Account Name", key: "acctName" },
        { title: "Account No", key: "acctNo" },
        {
          title: "Created at",
          key: "createdDate",
          transform: (fieldData, rowData) =>
            new Date(fieldData).toLocaleDateString(),
        },
        {
          title: "Paid at",
          key: "paymentDate",
          transform: (fieldData, rowData) =>
            new Date(fieldData).toLocaleDateString(),
        },
        {
          title: "Status",
          key: "paymentStatus",
          transform: (fieldData, rowData) => {
            if (fieldData === 0) {
              return (fieldData = "Pending");
              // fieldData.toString().replace(fieldData, "Not Paid");
            } else if (fieldData === 1) {
              return (fieldData = "Confirmed");
              // fieldData.toString().replace(fieldData, "Paid");
            }
          },
        },
        // { title: "Action", key: "action" },
      ],
      options: {},
      // options: {
      //   bulkActions: [""],
      //   // singleActions: ["Confirm Payment"],
      //   singleActions: {
      //     key: "Confirm Payment",
      //     transform: (fieldData) => {
      //       console.log(fieldData, "data");
      //       // if (fieldData === 0) {
      //       //   return (fieldData = "Pending");
      //       //   // fieldData.toString().replace(fieldData, "Not Paid");
      //       // } else if (fieldData === 1) {
      //       //   return (fieldData = "Confirmed");
      //       //   // fieldData.toString().replace(fieldData, "Paid");
      //       // }
      //     },
      //   },
      //   // singleActions: ["Confirm Payment"],
      //   // transform: (fieldData) => {
      //   //   console.log(fieldData, "data");
      //   // },
      // },
    },
  };
  flow: any = {
    currentId: 0,
    billing: {
      options: {
        title: "Billing Details",
        type: "detail",
      },
      tenantId: 0,
      display: false,
    },
  };
  display: any = {
    showSpinner: false,
    dashboard: false,
    transaction: false,
    // score: false,
  };
  navMenu: any = {
    default: "dashboard",
    list: [
      { name: "dashboard", selected: true },
      { name: "transaction", selected: false },
      // { name: "details", selected: false },
    ],
  };
  loader: any = {
    dataless: {
      title: "No Data Available!",
      subTitle: "Please create a contact and try again",
      action: "Create Contact",
      success: true,
      exists: true,
    },
    main: {
      default: "sharp",
      spinnerType: "",
      spinnerStyle: { marginTop: "15%" },
      showSpinner: false,
    },
    flow: {
      type: "sharp",
      status: "Loading...",
      styles: { marginTop: "15%" },
      showSpinner: false,
    },
    content: {
      type: "sharp",
      status: "Loading...",
      styles: { marginTop: "10%" },
      showSpinner: false,
    },
    export: false,
    paymentBtn: false,
  };
  id: number;
  dataSource: any = [];
  dataTransaction: any = [];
  isOpened: boolean = false;
  invoiceForm: FormGroup;
  bankPaymentForm: FormGroup;
  selectedClientName = "none";
  selectedCurrency: string;
  arrayTenants: any;
  arrayLicenses: any = [];
  arrayAccounts: any = [];
  arrayCards: any = [];
  isLoading: boolean = false;
  billingData: any;
  balanceAmount: number = 0;
  totalAmount: number = 0;
  amountPaid: number = 0;
  paymentMethod: any;
  selectedCard = "bank";
  ngModelCard: any = {};
  ngModelBalance: any;
  currentUser: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private authService: AuthService,
    private settingSvc: SettingsService,
    private sharedService: SharedService,
    private licenseService: LicenseService,
    private tenantsService: TenantsService,
    private billingInvoiceService: BillingInvoiceService
  ) {
    this.route.params.subscribe((params) => {
      const { id } = params;
      this.id = id;
      console.log(id);
      this.currentUser = this.authService.currentUserValue;
      // console.log("currentUser", currentUser);
    });
  }

  async ngOnInit() {
    this.ngForms();
    await this.ngOnLoad(this.id);
    await this.getAllBankAccountsAsync();
  }

  ngAfterViewInit() {
    this.openTemplate("billing-details-1");
  }

  async ngOnLoad(id) {
    if (!id) {
      this.toastr.error("No data for display!");
      return;
    }
    await this.getBillingDetailsAsync(id);
    this.setFormValues(this.dataSource);

    console.log(this.invoiceForm.value, "form values");

    //set content settings
    const defaultView = "dashboard";
    await this.onSelectMenu(defaultView);
    await this.getAllOrganizationBillingPaymentsAsync();
    await this.getBankPaymentsByBillingID(this.id);
    await this.calculateSummary();
  }

  // DATATABLE
  dataFeedBackObsListener = async (data) => {
    console.log(data);
    switch (data.type) {
      case "bulkactions":
        // if (data.action === "Export") await this.exportTable();
        break;
      case "singleaction":
        if (data.action === "Confirm Payment") {
          //await this.confirmPaymentTransaction();
          // this.router.navigate(["/billing-invoice/edit/" + data.data.id]);
        }
        break;

      default:
        break;
    }
  };

  // convenience getter for easy access to form fields
  get f() {
    return this.invoiceForm.controls;
  }

  get bf() {
    return this.bankPaymentForm.controls;
  }

  ngForms() {
    let today = new Date();

    this.invoiceForm = this.fb.group({
      tenantID: [{ value: "", disabled: true }, Validators.required],
      tenantName: [{ value: "", disabled: true }, Validators.required],
      tenantEmail: [{ value: "", disabled: true }, Validators.required],
      currency: [{ value: "", disabled: true }, Validators.required],
      paymentMethod: [{ value: "", disabled: false }, Validators.required],
      dueDate: [{ value: "", disabled: true }, Validators.required],
      license: [{ value: "", disabled: true }, Validators.required],
      licenseID: [{ value: "", disabled: true }, Validators.required],
      descrip: [{ value: "", disabled: true }, Validators.required],
      unitPrice: [{ value: "", disabled: true }, Validators.required],
      quantity: [{ value: "", disabled: true }, Validators.required],
      amount: [{ value: "", disabled: true }, Validators.required],
      type: [{ value: "", disabled: true }, Validators.required],
      totalAmount: [{ value: "", disabled: true }, Validators.required],
      autoRenew: false,
      extendedDuration: 0,
      status: [{ value: "", disabled: true }],
    });

    this.bankPaymentForm = this.fb.group({
      amount: ["", Validators.required],
      billingID: [Number(this.id), Validators.required],
      details: [""],
      reference: [""],
      paymentStatus: [0],
      paymentDate: today.toString(),
      setupID: ["", Validators.required], //selectedBankID
      whatMode: ["CASH"], //CASH or CARD
      // createdDate: new Date(),
    });
  }

  calculateSummary() {
    // Get amountPaid
    // this.totalAmount += d.amount;
    this.dataTransaction.filter(async (d) => {
      this.amountPaid += d.amount;
    });
    this.totalAmount = Number(this.f.totalAmount.value);
    // console.log(this.f.totalAmount, "totalAmount");

    // if (this.totalAmount && this.amountPaid) {
    this.balanceAmount = Number(this.totalAmount - this.amountPaid);
    this.ngModelBalance = this.balanceAmount;
    this.bf.amount.setValue(this.balanceAmount);
    // }
  }

  setFormValues(data: any) {
    data.paymentMethod = data.paymentMethod.toUpperCase();
    data.type = data.type.toUpperCase();
    this.invoiceForm.patchValue(data);
  }

  async getBillingDetailsAsync(id: number) {
    try {
      this.loader.flow.showSpinner = true;
      this.dataSource = await this.billingInvoiceService
        .getBillingById(id)
        .toPromise();
      console.log(this.dataSource);
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    } finally {
      this.loader.flow.showSpinner = false;
    }
  }

  async getBankPaymentsByBillingID(id: number) {
    try {
      this.dataTransaction = await this.settingSvc
        .getBankPaymentsByBillingID(id)
        .toPromise();
      // console.log(this.dataTransaction, "dataTransaction");
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    }
  }

  async getAllOrganizationsAsync() {
    try {
      this.arrayTenants = await this.tenantsService
        .getAllOrganizations()
        .toPromise();
      // console.log(this.arrayTenants, "arrayTenants");
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    }
  }

  async getAllBankAccountsAsync() {
    try {
      this.arrayAccounts = await this.settingSvc
        .getAllBankAccounts()
        .toPromise();
      // console.log(this.arrayAccounts, "arrayAccounts");
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    }
  }

  async getAllOrganizationBillingPaymentsAsync() {
    try {
      this.arrayCards = await this.settingSvc
        .getAllOrganizationBillingPayment(2)
        .toPromise();
      // console.log(this.arrayCards, "arrayCards");
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    }
  }

  async getLicensesAsync() {
    try {
      this.arrayLicenses = await this.licenseService.getLicenses().toPromise();
      // console.log(this.arrayLicenses, "arrayLicenses");
    } catch (error) {
      this.toastr.error("No data for display!");
      console.log(error);
    }
  }

  onMouseMenu() {
    this.isOpened = !this.isOpened;
  }

  //Filter Select Menu
  async onSelectMenu(menuName) {
    this.navMenu.default = menuName;
    this.navMenu.list.filter(async (m) => {
      m.selected = false;
      if (m.name === menuName) m.selected = true;
      await this.toggleView(menuName);
    });
    // console.log(this.contactMenus, "contactMenus");
  }

  private async toggleView(view) {
    let id = this.id;
    this.loader.content.showSpinner = true;
    // await this.getBankPaymentsByBillingID(this.id);

    switch (view) {
      case "dashboard":
        this.calculateSummary();
        await this.getLicensesAsync();
        await this.getAllOrganizationsAsync();
        this.display.dashboard = true;
        this.display.transaction = false;
        // this.display.score = false;
        break;

      case "transaction":
        await this.getBankPaymentsByBillingID(id);
        this.calculateSummary();
        this.display.dashboard = false;
        this.display.transaction = true;
        // this.display.score = false;
        break;

      default:
        this.calculateSummary();
        this.display.dashboard = false;
        this.display.transaction = false;
        // this.display.score = false;
        this.loader.spinnerType = "errorCard";
        break;
    }
    setTimeout(async () => (this.loader.content.showSpinner = false), 1000);
  }

  openTemplate(id: string) {
    console.log(id, "id");
    this.sharedService.open(id);
  }

  closeTemplate(id: string) {
    this.sharedService.close(id);
  }

  //
  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  onChangeTenant(): void {
    const tenantID = this.invoiceForm.get("tenantID").value;
    this.arrayTenants.filter((res) => {
      if (Number(res.id) === Number(tenantID)) {
        //Set tenant name
        this.invoiceForm.get("tenantName").setValue(res.name);
      }
    });
  }

  onChangeCurrency(): void {
    const currency = this.invoiceForm.get("currency").value;
    this.selectedCurrency = currency;
  }

  onChangePaymentMethod(): void {
    this.paymentMethod = this.invoiceForm.get("paymentMethod").value;
    let days: number;
    let today = new Date();

    if (this.paymentMethod === "MONTHLY") {
      days = 30;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "QUARTERLY") {
      days = 90;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "ANNUALLY") {
      days = 365;
      today = this.addDays(today, days);
    }
    this.invoiceForm.get("dueDate").setValue(today);

    if (this.f.unitPrice.valid) {
      this.loader.overlay = true;
      this.onChangeLicense();
      new Promise((resolve) => {
        setTimeout(() => {
          this.loader.overlay = false;
          resolve();
        }, 2000);
      });
    }
  }

  onChangeLicense(): void {
    const licenseID = this.invoiceForm.get("licenseID").value;
    let unitPrice;

    this.arrayLicenses.filter((res) => {
      if (Number(res.id) === Number(licenseID)) {
        this.invoiceForm.get("descrip").setValue(res.descrip); //Set descrip
        this.invoiceForm.get("license").setValue(res.name); //Set license name
        this.invoiceForm.get("quantity").setValue("1"); //Set Quantity

        if (this.paymentMethod === "MONTHLY") {
          unitPrice = res.monthlyPrice;
        } else if (this.paymentMethod === "quarterly") {
          unitPrice = res.quarterlyPrice;
        } else if (this.paymentMethod === "ANNUALLY") {
          unitPrice = res.annuallyPrice;
        }

        this.invoiceForm.get("unitPrice").setValue(unitPrice); //Set unitPrice
      }
    });
  }

  onUpdate() {
    this.isLoading = true;
    this.processForm();

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 2000);
  }

  processForm() {
    let payload = this.invoiceForm.value;
    payload = {
      ...payload,
      id: this.billingData.id,
      status: this.billingData.status,
    };
    this.billingInvoiceService.editBilling(payload).subscribe(
      (res) => {
        this.router.navigate(["billing-invoice/list"]);
        this.toastr.success("Invoice updated successfully!");
      },
      (err) => this.toastr.error("Error updating invoice. Please try again!")
    );
  }

  processBankPayment() {
    this.loader.paymentBtn = true;
    let payload = this.bankPaymentForm.value;
    // let ref = new Date().getTime().toString(36);
    let ref = "ABC" + new Date().valueOf();
    payload = {
      ...payload,
      reference: ref,
      setupID: Number(payload.setupID),
    };

    this.settingSvc
      .newPayment(payload)
      .subscribe(
        (res) => {
          // this.router.navigate(["billing-invoice/list"]);
          this.toastr.success("Bank payment updated successfully!");
          this.onSelectMenu("transaction");
          this.bankPaymentForm.reset();
        },
        (err) =>
          this.toastr.error("Error updating bank payment. Please try again!")
      )
      .add(() => {
        this.loader.paymentBtn = false;
      });
  }

  async processCardPayment() {
    try {
      this.loader.paymentBtn = true;
      const billId = this.id;
      const cardId = this.ngModelCard;
      await this.settingSvc.chargeCardForBill(billId, cardId).toPromise();
      this.toastr.success("Billing payment successful!", "Success");
      await this.onSelectMenu("transaction");
    } catch (error) {
      this.toastr.error(
        "Error processing payment, please try again!",
        "Failure"
      );
      // console.log(error, "err");
    } finally {
      this.loader.paymentBtn = false;
    }
  }
}
