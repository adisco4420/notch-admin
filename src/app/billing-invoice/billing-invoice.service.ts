import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { retry, catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Billing } from './billing-invoice.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class BillingInvoiceService {

  url: string;
  clientsURL = `${environment.apiUrl}3200`;
  clients = {
    getOneGetAllClients: '/clients/',
    mergeCompanyContact: '/clients/merger',
    // getAllByPaginationClients: "/clients/filter?page=0&population=20",
    // filterClients: "/clients/filter",
  };


  constructor(private appService: AppService, private http: HttpClient) { }

  getBillings() {
    this.url = `${this.appService.baseURL}/getBillings/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.appService.handleError)
      );
  }

  getBillingById(id: number) {
    this.url = `${this.appService.baseURL}/${id}/getBilling/`;
    return this.appService.get(this.url)
      .pipe(
        retry(1),
        catchError(this.appService.handleError)
      );
  }

  newBilling(billing: Billing) {
    this.url = `${this.appService.baseURL}/newBilling/`;
    return this.appService.post(this.url, billing);
  }

  editBilling(billing: Billing) {
    this.url = `${this.appService.baseURL}/editBilling/`;
    return this.appService.post(this.url, billing);
  }

  deleteBilling(id: number) {
    this.url = `${this.appService.baseURL}/${id}/deleteBilling/`;
    return this.appService.delete(this.url);
  }

  getAllClients() {
    this.url = this.clientsURL + this.clients.getOneGetAllClients;
    const options = { headers: this.getHeaders() };

    return this.http.get(`${this.url}`, options).pipe(
      retry(1),
      catchError(this.appService.handleError)
    );
  }

  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'orgId': 'orgId 1',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    return headers;
  }
}
