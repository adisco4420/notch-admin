import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BillingInvoiceListComponent } from "./billing-invoice-list/billing-invoice-list.component";
import { CreateInvoiceBillComponent } from "./create-invoice-bill/create-invoice-bill.component";
import { EditInvoiceBillComponent } from "./edit-invoice-bill/edit-invoice-bill.component";
import { LicenseStatusComponent } from "./license-status/license-status.component";
import { BillingDetailsComponent } from "./billing-details/billing-details.component";

const routes: Routes = [
  {
    path: "list",
    component: BillingInvoiceListComponent,
  },
  {
    path: "create",
    component: CreateInvoiceBillComponent,
  },
  {
    path: "edit/:id",
    component: EditInvoiceBillComponent,
  },
  {
    path: ":id",
    component: BillingDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillingInvoiceRoutingModule {}
