import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { TenantsService } from "./../../tenants/tenants.service";
import { LicenseService } from "./../../license/license.service";
import { BillingInvoiceService } from "../billing-invoice.service";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
} from "@angular/core";
import { Router } from "@angular/router";
import { License } from "src/app/license/license.model";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-create-invoice-bill",
  templateUrl: "./create-invoice-bill.component.html",
  styleUrls: ["./create-invoice-bill.component.css"],
})
export class CreateInvoiceBillComponent implements OnInit {
  @Input() tenantId: number;
  @Output() eventSource: EventEmitter<any> = new EventEmitter<any>(null);
  selectedClientName = "none";
  selectedCurrency: string;
  arrayTenants: any;
  arrayLicenses: License[];
  calculatedAmount: number;
  totalAmount: number = 0.0;
  paymentMethod: any;

  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  isLoading: boolean = false;
  invoiceForm: FormGroup;
  loader: any = {
    dataless: {
      title: "No Data Available!",
      subTitle: "Please create a contact and try again",
      action: "Create Contact",
      success: true,
      exists: true,
    },
    main: {
      default: "sharp",
      spinnerType: "",
      spinnerStyle: { marginTop: "15%" },
      showSpinner: false,
    },
    overlay: false,
  };
  options = [
    {
      label: "Angular 2",
      value: "angular2",
    },
    {
      label: "ReactJS",
      value: "reactjs",
    },
  ];
  config = {
    labelField: "label",
    valueField: "value",
    highlight: false,
    create: false,
    persist: true,
    plugins: ["dropdown_direction", "remove_button"],
    dropdownDirection: "down",
    maxItems: 5,
  };
  placeholder: string = "Placeholder...";
  value: string[];

  constructor(
    private billingInvoiceService: BillingInvoiceService,
    private licenseService: LicenseService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private tenantsService: TenantsService,
    private router: Router
  ) {
    this.loader.main.spinnerType = "sharp";
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "";
    this.createForm();
  }

  ngOnInit() {
    this.getAllOrganizations();
    this.getLicenses();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.setTenantId(this.tenantId);
  }

  onValueChange($event) {
    console.log("Option changed: ", $event);
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.invoiceForm.controls;
  }

  createForm() {
    this.invoiceForm = this.fb.group({
      tenantID: ["", Validators.required],
      tenantName: ["", Validators.required],
      tenantEmail: ["", Validators.required],
      currency: ["", Validators.required],
      paymentMethod: ["", Validators.required],
      dueDate: ["", Validators.required],
      license: ["", Validators.required],
      licenseID: ["", Validators.required],
      descrip: ["", Validators.required],
      unitPrice: ["", Validators.required],
      quantity: ["", Validators.required],
      amount: ["", Validators.required],
      type: ["", Validators.required],
      totalAmount: ["", Validators.required],
      autoRenew: true,
      extendedDuration: 0,
      status: "status",
    });
  }

  setTenantId(id) {
    console.log(id, "id");
    if (!id) return;
    this.invoiceForm.get("tenantID").setValue(id);
    this.onChangeTenant();
  }

  getAllOrganizations() {
    this.tenantsService.getAllOrganizations().subscribe(
      (data: any) => {
        this.arrayTenants = data;
        this.showSpinner = false;
      },
      (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load the data.";
      }
    );
  }

  getLicenses() {
    this.licenseService.getLicenses().subscribe(
      (data: any) => {
        this.arrayLicenses = data;
        this.showSpinner = false;
      },
      (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load the data.";
      }
    );
  }

  onChangeTenant(): void {
    const tenantID = this.invoiceForm.get("tenantID").value;
    this.arrayTenants.filter((res) => {
      if (Number(res.id) === Number(tenantID)) {
        console.log(res, "res");

        //Set tenant info
        this.invoiceForm.get("tenantName").setValue(res.name);
        this.invoiceForm.get("tenantEmail").setValue(res.email);
      }
    });
  }

  onChangeCurrency(): void {
    const currency = this.invoiceForm.get("currency").value;
    this.selectedCurrency = currency;

    if (this.f.license.valid) {
      this.onChangeLicense();
    }
  }

  onChangePaymentMethod(): void {
    this.paymentMethod = this.invoiceForm.get("paymentMethod").value;
    let days: number;
    let today = new Date();

    if (this.paymentMethod === "MONTHLY") {
      days = 30;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "QUARTERLY") {
      days = 90;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "ANNUALLY") {
      days = 365;
      today = this.addDays(today, days);
    }
    this.invoiceForm.get("dueDate").setValue(today);

    if (this.f.unitPrice.valid) {
      this.loader.overlay = true;
      this.onChangeLicense();
      new Promise((resolve) => {
        setTimeout(() => {
          this.loader.overlay = false;
          resolve();
        }, 2000);
      });
    }
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  onChangeLicense(): void {
    const licenseID = this.invoiceForm.get("licenseID").value;
    let unitPrice;

    this.arrayLicenses.filter((res) => {
      if (Number(res.id) === Number(licenseID)) {
        this.invoiceForm.get("descrip").setValue(res.descrip); //Set descrip
        this.invoiceForm.get("license").setValue(res.name); //Set license name
        this.invoiceForm.get("quantity").setValue("1"); //Set Quantity

        if (this.paymentMethod === "MONTHLY") {
          if (this.selectedCurrency === "NGN") unitPrice = res.monthlyPrice;
          else if (this.selectedCurrency === "USD")
            unitPrice = res.monthlyPriceUSD;
        } else if (this.paymentMethod === "ANNUALLY") {
          if (this.selectedCurrency === "NGN") unitPrice = res.annuallyPrice;
          else if (this.selectedCurrency === "USD")
            unitPrice = res.annuallyPriceUSD;
        }

        this.invoiceForm.get("unitPrice").setValue(unitPrice); //Set unitPrice
        this.invoiceForm.get("amount").setValue(unitPrice); //Set amount
        this.invoiceForm.get("totalAmount").setValue(unitPrice); //Set totalAmount
      }
    });
  }

  // Computation of Invoice By Row
  handleComputation(): void {
    this.handleTotalAmountComputation();
    this.calculateAmountAfterTax();
  }

  calculateAmountAfterTax() {
    const taxType = this.invoiceForm.get("tax").value;
    const amount = this.invoiceForm.get("amount").value;
    const taxRate = 5 / 100;
    let calculatedTax: number;
    let calculatedAmount: number;

    if (taxType && Number(amount)) {
      if (taxType === "none") {
        calculatedTax = 0;
        this.invoiceForm.get("taxAmount").setValue(calculatedTax);
      } else if (taxType === "vat") {
        calculatedTax = taxRate * Number(amount);
        this.invoiceForm.get("taxAmount").setValue(calculatedTax);
        // console.log(calculatedTax, 'calculatedTax');

        calculatedAmount = Number(amount) + Number(calculatedTax);
        this.invoiceForm.get("amount").setValue(calculatedAmount);
      }
    }
  }

  // Computation of Total Invoice
  handleTotalAmountComputation() {
    const unitPrice = this.invoiceForm.get("unitPrice").value;
    const quantity = this.invoiceForm.get("quantity").value;
    const tax = this.invoiceForm.get("tax").value;
    let calculatedAmount: number;

    if (Number(unitPrice) && Number(quantity)) {
      calculatedAmount = Number(unitPrice) * Number(quantity);
      this.invoiceForm.get("amount").setValue(calculatedAmount);
      this.invoiceForm.get("totalAmount").setValue(calculatedAmount);
      this.totalAmount = Number(calculatedAmount);
    }
  }

  handleTaxSelection() {}

  onSubmit() {
    this.isLoading = true;
    this.processForm();

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 2000);
  }

  processForm() {
    const payload = this.invoiceForm.value;
    this.billingInvoiceService.newBilling(payload).subscribe(
      (res) => {
        this.router.navigate(["billing-invoice/list"]);
        this.toastr.success("Invoice created successfully!", "Success");
        this.eventSource.emit({ action: "createInvoiceSuccess", data: res });
      },
      (err) => {
        this.toastr.error("Error updating invoice.", "Please try again!");
        this.eventSource.emit({ action: "createInvoiceFailed", data: [] });
      }
    );
  }
}
