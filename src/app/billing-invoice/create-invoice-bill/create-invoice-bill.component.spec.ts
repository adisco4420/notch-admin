import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInvoiceBillComponent } from './create-invoice-bill.component';

describe('CreateInvoiceBillComponent', () => {
  let component: CreateInvoiceBillComponent;
  let fixture: ComponentFixture<CreateInvoiceBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInvoiceBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInvoiceBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
