import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInvoiceBillComponent } from './edit-invoice-bill.component';

describe('EditInvoiceBillComponent', () => {
  let component: EditInvoiceBillComponent;
  let fixture: ComponentFixture<EditInvoiceBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInvoiceBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInvoiceBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
