import { Component, OnInit } from "@angular/core";
import { License } from "src/app/license/license.model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BillingInvoiceService } from "../billing-invoice.service";
import { LicenseService } from "src/app/license/license.service";
import { TenantsService } from "src/app/tenants/tenants.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-edit-invoice-bill",
  templateUrl: "./edit-invoice-bill.component.html",
  styleUrls: ["./edit-invoice-bill.component.css"],
})
export class EditInvoiceBillComponent implements OnInit {
  selectedClientName = "none";
  selectedCurrency: string;
  arrayTenants: any;
  arrayLicenses: License[];
  billingData: any;
  calculatedAmount: number;
  totalAmount: number = 0.0;
  paymentMethod: any;
  loader: any = {
    overlay: false,
  };
  spinnerType: string;
  setSpinnerStatus: string;
  showSpinner: boolean = true;
  isLoading: boolean = false;
  invoiceForm: FormGroup;
  bid: number;

  constructor(
    private billingInvoiceService: BillingInvoiceService,
    private toastr: ToastrService,
    private licenseService: LicenseService,
    private tenantsService: TenantsService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spinnerType = "jsBin";
    this.setSpinnerStatus = "";

    this.route.params.subscribe((params) => {
      this.bid = params["id"];
      this.getBillingById(this.bid);
    });
  }

  ngOnInit() {
    this.getAllOrganizations();
    this.getLicenses();
    this.createForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.invoiceForm.controls;
  }

  createForm() {
    this.invoiceForm = this.fb.group({
      tenantID: ["", Validators.required],
      tenantName: ["", Validators.required],
      tenantEmail: ["", Validators.required],
      currency: ["", Validators.required],
      paymentMethod: ["", Validators.required],
      dueDate: ["", Validators.required],
      license: ["", Validators.required],
      licenseID: ["", Validators.required],
      descrip: ["", Validators.required],
      unitPrice: ["", Validators.required],
      quantity: ["", Validators.required],
      tax: ["", Validators.required],
      taxAmount: ["", Validators.required],
      amount: ["", Validators.required],
      type: ["", Validators.required],
      totalAmount: ["", Validators.required],
    });
  }

  getBillingById(id: number) {
    this.billingInvoiceService.getBillingById(id).subscribe((data: any) => {
      this.setFormValues(data);
      this.billingData = data;
      this.showSpinner = false;
    });
  }

  getAllOrganizations() {
    this.tenantsService.getAllOrganizations().subscribe(
      (data: any) => {
        this.arrayTenants = data;
        this.showSpinner = false;
      },
      (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load the data.";
      }
    );
  }

  getLicenses() {
    this.licenseService.getLicenses().subscribe(
      (data: any) => {
        this.arrayLicenses = data;
        this.showSpinner = false;
      },
      (err) => {
        this.spinnerType = "errorCard";
        this.setSpinnerStatus = "We couldn't load the data.";
      }
    );
  }

  setFormValues(data) {
    this.invoiceForm.patchValue({
      tenantID: data.tenantID,
      tenantName: data.tenantName,
      tenantEmail: data.tenantEmail,
      currency: data.currency,
      paymentMethod: data.paymentMethod,
      dueDate: data.dueDate,
      license: data.license,
      licenseID: data.licenseID,
      descrip: data.descrip,
      unitPrice: data.unitPrice,
      quantity: data.quantity,
      tax: data.tax,
      taxAmount: data.taxAmount,
      amount: data.amount,
      type: data.type,
      totalAmount: data.totalAmount,
    });
  }

  onChangeTenant(): void {
    const tenantID = this.invoiceForm.get("tenantID").value;
    this.arrayTenants.filter((res) => {
      if (Number(res.id) === Number(tenantID)) {
        //Set tenant name
        this.invoiceForm.get("tenantName").setValue(res.name);
      }
    });
  }

  onChangeCurrency(): void {
    const currency = this.invoiceForm.get("currency").value;
    this.selectedCurrency = currency;
  }

  onChangePaymentMethod(): void {
    this.paymentMethod = this.invoiceForm.get("paymentMethod").value;
    let days: number;
    let today = new Date();

    if (this.paymentMethod === "monthly") {
      days = 30;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "quarterly") {
      days = 90;
      today = this.addDays(today, days);
    } else if (this.paymentMethod === "annually") {
      days = 365;
      today = this.addDays(today, days);
    }
    this.invoiceForm.get("dueDate").setValue(today);

    if (this.f.unitPrice.valid) {
      this.loader.overlay = true;
      this.onChangeLicense();
      new Promise((resolve) => {
        setTimeout(() => {
          this.loader.overlay = false;
          resolve();
        }, 2000);
      });
    }
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  onChangeLicense(): void {
    const licenseID = this.invoiceForm.get("licenseID").value;
    let unitPrice;

    this.arrayLicenses.filter((res) => {
      if (Number(res.id) === Number(licenseID)) {
        this.invoiceForm.get("descrip").setValue(res.descrip); //Set descrip
        this.invoiceForm.get("license").setValue(res.name); //Set license name
        this.invoiceForm.get("quantity").setValue("1"); //Set Quantity

        if (this.paymentMethod === "monthly") {
          unitPrice = res.monthlyPrice;
        } else if (this.paymentMethod === "quarterly") {
          unitPrice = res.quarterlyPrice;
        } else if (this.paymentMethod === "annually") {
          unitPrice = res.annuallyPrice;
        }

        this.invoiceForm.get("unitPrice").setValue(unitPrice); //Set unitPrice
      }
    });
  }

  // Computation of Invoice By Row
  handleComputation(): void {
    this.handleTotalAmountComputation();
    this.calculateAmountAfterTax();
  }

  calculateAmountAfterTax() {
    const taxType = this.invoiceForm.get("tax").value;
    const amount = this.invoiceForm.get("amount").value;
    const taxRate = 5 / 100;
    let calculatedTax: number;
    let calculatedAmount: number;

    if (taxType && Number(amount)) {
      if (taxType === "none") {
        calculatedTax = 0;
        this.invoiceForm.get("taxAmount").setValue(calculatedTax);
      } else if (taxType === "vat") {
        calculatedTax = taxRate * Number(amount);
        this.invoiceForm.get("taxAmount").setValue(calculatedTax);
        // console.log(calculatedTax, 'calculatedTax');

        calculatedAmount = Number(amount) + Number(calculatedTax);
        this.invoiceForm.get("amount").setValue(calculatedAmount);
      }
    }
  }

  // Computation of Total Invoice
  handleTotalAmountComputation() {
    const unitPrice = this.invoiceForm.get("unitPrice").value;
    const quantity = this.invoiceForm.get("quantity").value;
    const tax = this.invoiceForm.get("tax").value;
    let calculatedAmount: number;

    if (Number(unitPrice) && Number(quantity)) {
      calculatedAmount = Number(unitPrice) * Number(quantity);
      this.invoiceForm.get("amount").setValue(calculatedAmount);
      this.invoiceForm.get("totalAmount").setValue(calculatedAmount);
      this.totalAmount = Number(calculatedAmount);
    }
  }

  handleTaxSelection() {}

  onUpdate() {
    this.isLoading = true;
    this.processForm();

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 2000);
  }

  processForm() {
    let payload = this.invoiceForm.value;
    payload = {
      ...payload,
      id: this.billingData.id,
      status: this.billingData.status,
    };
    this.billingInvoiceService.editBilling(payload).subscribe(
      (res) => {
        this.router.navigate(["billing-invoice/list"]);
        this.toastr.success("Invoice updated successfully!");
      },
      (err) => this.toastr.error("Error updating invoice. Please try again!")
    );
  }
}
